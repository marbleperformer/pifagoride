def _init_(app):

    app.opened_functions = list()
    

    def close_current_tab(index):

        app.window._tabs.setCurrentIndex(index)

        widget = app.window._tabs.currentWidget()

        app.window.remove_central_widget(widget)

        if widget.name and widget.closed:

            app.opened_functions.remove((app.repository.path, widget.name))


    app.window._tabs.tabCloseRequested.connect(close_current_tab)
    
