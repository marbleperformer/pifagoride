from appcore.executor.executor import Executor

from guicore.resultviewer.resultviewer import SimpleViewer


def _init_(app):

    app.result_viewer = SimpleViewer()

    app.window.add_doc(app.result_viewer, app.window.BOTTOMDOC)

    executor = Executor('executor.conf')

    menu = app.window.create_menu('Execute')

    for executable in executor.executables:

        action = menu.create_action(executable.name, executable, executable.title)

        app.window.add_tool(action)

        executable.add_observer(app.result_viewer)
