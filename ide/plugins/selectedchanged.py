import os

from appcore.executor.executor import Executor


def _init_(app):

    executor = Executor('executor.conf')

    def selected_changed():

        repo_path = app.repository.path

        repo_name = os.path.basename(repo_path)

        func_name = app.selected.name 

        func_path = app.repository._get_function_path(func_name)

        rig_path = app.repository._get_rig_path(func_name)

        cg_path = app.repository._get_cg_path(func_name)

        executor.set_variable('repository_name', repo_name)

        executor.set_variable('function_name', func_name)

        executor.set_variable('function_path', func_path)

        executor.set_variable('rig_path', rig_path)

        executor.set_variable('cg_path', cg_path)


    app.window._tabs.currentChanged.connect(selected_changed)

    app.window.listview.clicked.connect(selected_changed)

