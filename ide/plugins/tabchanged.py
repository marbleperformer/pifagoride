from PyQt5.QtCore import pyqtSlot


def _init_(app):

    @pyqtSlot(int)
    def change_selected(idx):

        if idx != -1:

            # widget = app.opened_functions[idx]

            widget = app.window._tabs.widget(idx)

            app.selected = widget


    app.window._tabs.currentChanged.connect(change_selected)
