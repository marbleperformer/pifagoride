from appcore.repositorymanager.repositorymanager import RepositoryManager

from guicore.createdialogs.createdialogs import RepositoryDialog

from guicore.listview.listview import ListView


def _init_(app):

    app.window.listview = ListView(app.window)

    def init_repository():

        path = RepositoryDialog.open_repository()

        if path:

            app.repository = RepositoryManager.init_repository(path)

            functions = app.repository.functions

            app.window.listview.init_repository(path, functions)


    file_menu = app.window.get_menu('file')

    open_repository_action = file_menu.create_action('open_repo', init_repository, 'Open reposotory')

    ListView.add_static_action(open_repository_action)

    app.window.add_doc(app.window.listview, app.window.LEFTDOC)



