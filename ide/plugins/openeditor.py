from PyQt5.QtCore import pyqtSlot

from PyQt5.QtWidgets import QMessageBox

from guicore.highlight.highlight import HighlightEditor

from appcore.repositorymanager.repositorymanager import ValidationError


def _init_(app):

    @pyqtSlot(str)
    def save_changes(content):

        selected = app.window.listview.selected_item

        name = selected.name

        app.repository.write_function(name, content)

        # try:

        #     app.repository.update_function(path, content)

        # except ValidationError as e:

        #     answer = QMessageBox.question('Rename function', 'Would you like to rename function?', QMessageBox.Yes | QMessageBox.No)

        #     if answer == QMessageBox.Yes:

        #         print('rename file')

        #     else:

        #         print('rename context')


    def init_editor(name):

        editor = HighlightEditor(name)

        editor.close_accepted.connect(save_changes)

        return editor


    def open_editor():

        selected_item = app.window.listview.selected_item

        if selected_item:

            name = selected_item.name

            if  (app.repository.path, name) in app.opened_functions:

                idx = app.opened_functions.index((app.repository.path, name))

                editor = app.window._tabs.widget(idx)

            else:

                editor = init_editor(name)

                app.opened_functions.append((app.repository.path, name))

                app.window.add_central_widget(editor, selected_item.name)

            content = app.repository.get_function_content(name)

            editor.setText(content)

            idx = app.window._tabs.indexOf(editor)

            app.window._tabs.setCurrentIndex(idx)

            
    app.window.listview.clicked.connect(open_editor)
