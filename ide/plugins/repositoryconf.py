import os

from appcore.repositorymanager.repositorymanager import RepositoryConfig


CONFIG_NAME = 'repository.conf'


def _init_(app):

    if os.path.exists(CONFIG_NAME):

        RepositoryConfig(CONFIG_NAME)
