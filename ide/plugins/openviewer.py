from PyQt5.QtWidgets import QMenu, QAction, QFileDialog

from guicore.highlight.highlight import HighlightViewer

from appcore.repositorymanager.repositorymanager import RepositoryManager


def _init_(app):

    def view_file():

        path, ext = QFileDialog.getOpenFileName(app.window, 'Select file', '/')

        if path:

            viewer = HighlightViewer()

            with open(path, 'r') as file:

                text = file.read()

            viewer.setText(text)

            app.window.add_central_widget(viewer, path)



    menu = app.window.create_menu('file')

    menu.create_action('view_file', func=view_file)
