
from guicore.highlight.highlight import HighlightEditor

from guicore.actions.actions import Action


def _init_(app):

    def save_changes():

        app.selected.save()

        name = app.selected.name

        content = app.selected.toPlainText()

        app.repository.write_function(name, content)


    save_action = Action('save', 'Save', app)

    save_action.setShortcut('Ctrl+S')

    save_action.bind(save_changes)

    HighlightEditor.add_static_action(save_action)