import re

import json

import subprocess

from configuration.configuration import ConfigMetaclass, ConfigAttribute

from collections import OrderedDict


class ExecutorConfig(metaclass=ConfigMetaclass):

    variable_pattern = ConfigAttribute('var_pattern', str, r'{(?P<key>(?P<type>[l,g,e,%]) (?P<name>\w+) [l,g,e,%])}')

    default_output_file = ConfigAttribute('out_file', str, 'output.txt')


    def __init__(self, path=None):

        if path:

            with open(path, 'r') as file:

                config_json = json.load(file)

                for name, value in config_json.items():

                    setattr(self, name, value)


class Executable(object):

    def __init__(self, name, title, group=None):

        self._observers = set()

        self._executor = Executor()

        self._title = title

        self._group = group

        self._name = name


    @property
    def name(self):

        return self._name


    @property
    def title(self):

        return self._title


    @property
    def group(self):

        return self._group


    def _notificate(self, output):

        for obs in self._observers:

            obs.__update__(output)


    def add_observer(self, observer):

        self._observers.add(observer)


    def remove_observer(self, observer):

        if not observer in self._observers:

            raise ValueError('Executable does not contain %s observer.' % observer) 

        self._observers.remove(observer)


    def __call__(self):

        exec_line = self._executor.get_exec_line(self._name)

        out_bytes = subprocess.check_output([exec_line], shell=True)

        out_string = out_bytes.decode('utf-8')

        clean_out = out_string.strip('\n')

        self._notificate(clean_out)


class Executor(metaclass=ConfigMetaclass):

    def __init__(self, path=None):

        self._execs = dict()

        self._instructions = dict()

        self._env_variables = dict()

        self._global_variables = dict()

        self._config = ExecutorConfig()


        if path:

            with open(path, 'r') as file:

                config_json = json.load(file)

                for name, value in config_json.items():

                    if isinstance(value, dict):

                        self._instructions[name] = value

                    else:

                        self._global_variables[name] = value


    @property
    def executables(self):

        for name, value in self._instructions.items():

            title = value.get('title')

            group = value.get('group')

            if not name in self._execs:

                executable = Executable(name, title, group)

                self._execs[name] = executable

            yield self._execs.get(name)


    def set_variable(self, name, value):

        self._env_variables[name] = value


    def set_variables(self, **variables):

        self._env_variables.update(variables)


    def get_exec_line(self, name):

        components = dict()

        instruction = self._instructions.get(name)

        exec_line = '\\'.join(instruction.get('exec_lines'))

        variables = instruction.get('variables') if 'variables' in instruction  else None

        matches = re.findall(self._config.variable_pattern, exec_line)

        for key, vtype, name in matches:

            if vtype == 'l':

                if not variables or not name in variables:

                    raise KeyError('You should set local variable into config before you can use it.')

                components[key] = variables.get(name)

            elif vtype == 'g':

                if not name in self._global_variables:

                    raise KeyError('You should set global variable into config before you can use it.')

                components[key] = self._global_variables.get(name)

            elif vtype == 'e':

                if not name in self._env_variables:

                    raise KeyError('You should set environment variable by set_variable or set_variables before you can use it.')

                components[key] = self._env_variables.get(name)

            elif vtype == '%':

                components[key] = self.get_exec_line(name) + '\\'

        return exec_line.format(**components)
