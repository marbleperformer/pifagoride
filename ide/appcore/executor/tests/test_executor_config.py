import pytest

import os

import json

from appcore.executor.executor import ExecutorConfig


@pytest.fixture
def pattern():

    return r'{(?P<key>(?P<type>[l,g,e,%]) (?P<name>\w+) [l,g,e,%])}'


@pytest.fixture
def output_file():

    return 'output.txt'


@pytest.fixture
def config_required(pattern, output_file):

    with open('testexecutor.conf', 'w') as file:

        json.dump({
            'variable_pattern':pattern,
            'default_output_file': output_file
        }, file)

    return ExecutorConfig('testexecutor.conf')


def teardown_function(function):

    ExecutorConfig._ExecutorConfig__instance = None

    if os.path.exists('testexecutor.conf'):

        os.remove('testexecutor.conf')


def test_variable_pattern(config_required, pattern):

    assert config_required.variable_pattern == pattern


def test_default_output_file(config_required, output_file):

    assert config_required.default_output_file == output_file
