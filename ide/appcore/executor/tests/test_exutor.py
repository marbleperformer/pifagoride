import pytest

import os

import json

from appcore.executor.executor import ExecutorConfig, Executor


@pytest.fixture
def trans_path():

    return 'path/to/trans2'


@pytest.fixture
def inter_path():

    return 'path/to/inter2'


@pytest.fixture
def gen_path():

    return 'path/to/cgen2'


@pytest.fixture
def trans_title():

    return 'Translate'


@pytest.fixture
def inter_title():

    return 'Interpretate'


@pytest.fixture
def gen_title():

    return 'Generate'



@pytest.fixture
def execute_group():

    return 'Execute'


@pytest.fixture
def repository_name():

    return 'repository'


@pytest.fixture
def repository_path(repository_name):

    return os.path.join('some/path/to/repo', repository_name)


@pytest.fixture
def selected_name():

    return 'test.function'


@pytest.fixture
def trans_instruction(trans_title, execute_group, trans_path):

    return {
        'title':trans_title,
        'group':execute_group,
        'variables': {
            'tool_path':trans_path
        },
        'exec_lines': [
            '{g trans_path g} -i {e repository_path e}/pfg/{e selected_name e}.pfg',
            '{l tool_path l}'
        ]
    }


@pytest.fixture
def gen_instruction(gen_title, execute_group):

    return {
        'title':gen_title,
        'group':execute_group,
        'exec_lines': [
            '{g gen_path g} -f {e repository_path e}/rig/{e selected_name e}.rig' '{e repository_path e}/cg/{e selected_name e}.cg'
        ]
    }


@pytest.fixture
def inter_instruction(inter_title, execute_group, inter_path):

    return {
        'title':inter_title,
        'group':execute_group,
        'variables': {
            'tool_path':inter_path
        },
        'exec_lines': [
            '{g inter_path g} {e selected_name e} {e repository_name e}'
        ]
    }


@pytest.fixture
def exec_instruction():

    return {
        'title':'Execute',
        'variables':{
        },
        'exec_lines':[
            '{% trans %} {% gen %} {% inter %}'
        ]

    }


@pytest.fixture
def wrong_instruction():

    return {
        'title':'Wrong',
        'exec_lines': [
            '{w wrong_type w}'
        ]
    }


@pytest.fixture
def notexists_env_instruction():

    return {
        'title':'Notexists',
        'exec_lines': [
            '{e notexistsenv e}'
        ]
    }

@pytest.fixture
def notexists_glob_instruction():

    return {
        'title':'Notexists',
        'exec_lines': [
            '{g notexistsglob g}'
        ]
    }


@pytest.fixture
def notexists_loc_instruction():

    return {
        'title':'Notexists',
        'exec_lines': [
            '{l notexistsloc l}'
        ]
    }


@pytest.fixture
def global_variables(trans_path, gen_path, inter_path):

    return {
        'trans_path':trans_path,
        'inter_path':inter_path,
        'gen_path':gen_path
    }


@pytest.fixture
def instructions(trans_instruction, gen_instruction, inter_instruction, exec_instruction, wrong_instruction, notexists_env_instruction, notexists_glob_instruction, notexists_loc_instruction):

    return {
        'trans':trans_instruction,
        'inter':inter_instruction,
        'gen':gen_instruction,
        'exec':exec_instruction,
        'wrong':wrong_instruction,
        'notexistsenv':notexists_env_instruction,
        'notexistsglob':notexists_glob_instruction,
        'notexistsloc':notexists_loc_instruction,
    }


@pytest.fixture
def instructions_len(instructions):

    return len(instructions)


@pytest.fixture
def config_path():

    return 'executions.conf'


@pytest.fixture
def executor(config_path, global_variables, instructions):

    config = {}

    config.update(global_variables)

    config.update(instructions)

    with open(config_path, 'w') as file:

        json.dump(config, file)

    return Executor(config_path)


@pytest.fixture
def set_variables_required(executor, trans_path, repository_path, selected_name):
    executor.set_variables(trans_path=trans_path, repository_path=repository_path, selected_name=selected_name)


@pytest.fixture
def trans_exec_line(trans_path, repository_path, selected_name):

    return '{trans_path} -i {repository_path}/pfg/{selected_name}.pfg\\{tool_path}'.format(trans_path=trans_path, repository_path=repository_path, selected_name=selected_name, tool_path=trans_path)


@pytest.fixture
def gen_exec_line(gen_path, repository_path, selected_name):

    return  '{gen_path} -f {repository_path}/rig/{selected_name}.rig' '{repository_path}/cg/{selected_name}.cg'.format(gen_path=gen_path, repository_path=repository_path, selected_name=selected_name)


@pytest.fixture
def inter_exec_line(inter_path, selected_name, repository_name):

    return '{inter_path} {selected_name} {repository_name}'.format(inter_path=inter_path, selected_name=selected_name, repository_name=repository_name)


@pytest.fixture
def exec_exec_line(trans_exec_line, gen_exec_line, inter_exec_line):

    return trans_exec_line + '\\ ' + gen_exec_line + '\\ ' + inter_exec_line + '\\'


def setup_function(function):

    ExecutorConfig()


def teardown_function(function):

    if os.path.exists('executions.conf'):

        os.remove('executions.conf')


def test_singleton_init(executor):

    new_executor = Executor()

    assert executor is new_executor


def test_global_variables_init(executor, global_variables):

    assert executor._global_variables == global_variables


def test_instructions_init(executor, instructions):

    assert executor._instructions == instructions


def test_names_executables(executor, instructions_len):

    execs = list(executor.executables)

    assert len(execs) == instructions_len


def test_set_variable(executor, selected_name):

    executor.set_variable('selected_name', selected_name)

    assert len(executor._env_variables) == 1


def test_set_variables(executor, selected_name, repository_name):

    executor.set_variables(selected_name=selected_name, repository_name=repository_name)

    assert len(executor._env_variables) == 2


def test_get_exec_line(executor, trans_exec_line, set_variables_required):

    line = executor.get_exec_line('trans')

    assert line == trans_exec_line

def test_exec_key_get_exec_line(executor, exec_exec_line, set_variables_required):

    line = executor.get_exec_line('exec')

    assert line == exec_exec_line


@pytest.mark.xfail(raises=KeyError)
def test_wrong_key_get_exec_line(executor):

    executor.get_exec_line('wrong')


@pytest.mark.xfail(raises=KeyError)
def test_notexists_env_variable_get_exec_line(executor):

    executor.get_exec_line('notexistsenv')


@pytest.mark.xfail(raises=KeyError)
def test_notexists_glob_variable_get_exec_line(executor):

    executor.get_exec_line('notexistsglob')


@pytest.mark.xfail(raises=KeyError)
def test_notexists_loc_variable_get_exec_line(executor):

    executor.get_exec_line('notexistsloc')
