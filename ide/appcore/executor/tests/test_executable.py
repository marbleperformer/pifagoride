import pytest

import os

import json

from appcore.executor.executor import Executable, Executor


@pytest.fixture
def name():

    return 'exec'


@pytest.fixture
def title():

    return 'Execute'


@pytest.fixture
def group():

    return 'Execute'


@pytest.fixture
def conf_path():

    return 'executions.conf'


@pytest.fixture
def executable(name, title, group):

    return Executable(name, title, group)


@pytest.fixture
def output():

    return 'some raw output'


@pytest.fixture
def observer():

    class Observer():

        def __update__(self, data):

            print(data)

    return Observer()


@pytest.fixture
def exec_instruction(name, output):

    return {
        'title':name,
        'exec_lines': [
            'echo %s' % output
        ]
    }


@pytest.fixture
def config_required(name, title, exec_instruction, conf_path):

    with open(conf_path, 'w') as file:

        json.dump({
            'exec':exec_instruction
        }, file)

    Executor(conf_path)


@pytest.fixture
def added_observer_required(executable, observer):

    executable.add_observer(observer)


def teardown_function(function):

    Executor._ConfigMetaclass__instance = None


def test_name(executable, name):

    assert executable.name == name


def test_title(executable, title):

    assert executable.title == title


def test_group(executable, group):

    assert executable.group == group


def test_add_observer(executable, observer):

    executable.add_observer(observer)

    assert len(executable._observers) == 1


def test_remove_observer(executable, observer, added_observer_required):

    executable.remove_observer(observer)

    assert len(executable._observers) == 0


@pytest.mark.xfail(raises=ValueError)
def test_fail_remove_observer(executable, observer):

    executable.remove_observer(observer)


def test_notificate(executable, output, capsys, added_observer_required):

    executable._notificate(output)

    out, err = capsys.readouterr()

    assert out == output + '\n'


def test_call(config_required, executable, output, capsys, added_observer_required):

    executable()

    out, err = capsys.readouterr()

    assert out == output + '\n'
