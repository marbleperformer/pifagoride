import os

import shutil

import unittest

from collections import OrderedDict

import filemanager

# TEST REPOSITORY CONSTANTS

REPO_NAME = 'repository'
REPO_PATH = '/home/marble/pifagoride/%s' % REPO_NAME

RENAME_REPO_NAME = 'new_repo'
RENAME_REPO_PATH = '/home/marble/pifagoride/%s' % RENAME_REPO_NAME

NEW_REPO_NAME = 'newrepository'
NEW_REPO_PATH = '/home/marble/pifagoride/%s' % NEW_REPO_NAME

WRONG_REPO_NAME = 'wrongname'
WRONG_REPO_PATH = '/home/marble/pifagoride/%s' % WRONG_REPO_NAME

# TEST FUNCTIONS CONSTANTS

DEFAULT_FUNCTION_RANG = '00.00'
DEFAULT_FUNCTION_FILE_NAME = '1.pfg'

FUNCTION_NAME = 'func_1'
FUNCTION_PATH = os.path.join(REPO_PATH, FUNCTION_NAME)

NEW_FUNCTION_NAME = 'new_function'
NEW_FUNCTION_PATH = os.path.join(REPO_PATH, NEW_FUNCTION_NAME)

RENAME_FUNCTION_NAME = 'rename_function'
RENAME_FUNCTION_PATH = os.path.join(REPO_PATH, RENAME_FUNCTION_NAME)

COMPOSITE_FUNCTION_NAME = 'composite.function.name'

COMPOSITE_FUNCTION_PATH = os.path.join(REPO_PATH, COMPOSITE_FUNCTION_NAME.replace('.', '/'))

FUNCTION_FILE_PATH = os.path.join(REPO_PATH, FUNCTION_NAME, DEFAULT_FUNCTION_RANG, DEFAULT_FUNCTION_FILE_NAME)

WRONG_FUNCTION_FILE_PATH = os.path.join(WRONG_REPO_PATH, FUNCTION_NAME, DEFAULT_FUNCTION_RANG, DEFAULT_FUNCTION_FILE_NAME)

NOT_FUNCTION_FILE_NAME = 'test_file'

NOT_A_FUNCTION_NAME = 'not_a_function'
NOT_A_FUNCTION_PATH = os.path.join(REPO_PATH, NOT_A_FUNCTION_NAME)

MOVE_FUNCTION_NAME = 'move_function'
MOVE_FUNCTION_PATH = os.path.join(NEW_FUNCTION_PATH, MOVE_FUNCTION_NAME)

MOVE_ON_TOP_FUNCTION_NAME = '.'.join((NEW_FUNCTION_NAME, MOVE_FUNCTION_NAME))
MOVE_ON_TOP_FUNCTION_PATH = os.path.join(REPO_PATH, MOVE_FUNCTION_NAME)

DELETE_FUNCTION_NAME = 'delete_function'
DELETE_FUNCTION_PATH = os.path.join(REPO_PATH, DELETE_FUNCTION_NAME)

# TEST RANG CONSTANTS

NEW_FUNCTION_RANG = '00.01'
NEW_FUNCTION_RANG_PATH = os.path.join(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

NEW_RANG_DEFAULT_VALUE = 'Test set default rang.'

NEW_RANG_FUNCTION_FILE_PATH = os.path.join(REPO_PATH, NEW_FUNCTION_NAME, DEFAULT_FUNCTION_RANG, DEFAULT_FUNCTION_FILE_NAME)

GET_FILE_PATH_FILE_PATH = os.path.join(REPO_PATH, NEW_FUNCTION_PATH, DEFAULT_FUNCTION_RANG, DEFAULT_FUNCTION_FILE_NAME)

# TEST SCHEMA CONSTANTS

REPO_SCHEMA = OrderedDict([
    ('name', REPO_NAME),
    ('path', REPO_PATH),
    ('children', [
        OrderedDict([
            ('name', FUNCTION_NAME),
            ('path', FUNCTION_PATH)
        ])
    ])
])

NEW_REPO_SCHEMA = OrderedDict([
    ('name', NEW_REPO_NAME),
    ('path', NEW_REPO_PATH),
    ('children', [])
])

NEW_FUNCTION_SCHEMA = OrderedDict([
    ('name', NEW_FUNCTION_NAME),
    ('path', NEW_FUNCTION_PATH),
    ('children', [])
])

ALL_CHILDREN_FUNCTIONS_SCHEMA = OrderedDict([
    ('name', NEW_FUNCTION_NAME),
    ('path', NEW_FUNCTION_PATH),
    ('children', [
        OrderedDict([
            ('name', MOVE_FUNCTION_NAME),
            ('path', os.path.join(NEW_FUNCTION_PATH, MOVE_FUNCTION_NAME))
        ])
    ])
])

ALL_FUNCTION_RANGS_SCHEMA = OrderedDict([
    ('name', NEW_FUNCTION_NAME),
    ('path', NEW_FUNCTION_PATH),
    ('rangs', [
        OrderedDict([
            ('rang', DEFAULT_FUNCTION_RANG),
            ('path', os.path.join(NEW_FUNCTION_PATH, DEFAULT_FUNCTION_RANG))
        ]),
        OrderedDict([
            ('rang', NEW_FUNCTION_RANG),
            ('path', os.path.join(NEW_FUNCTION_PATH, NEW_FUNCTION_RANG))
        ])
    ])
])

# TODO setUp create test repository
class FileManagerTestCase(unittest.TestCase):
    def tearDown(self):
        if os.path.exists(NEW_REPO_PATH):
            shutil.rmtree(NEW_REPO_PATH)

        if os.path.exists(RENAME_REPO_PATH):
            shutil.rmtree(RENAME_REPO_PATH)

        if os.path.exists(NEW_FUNCTION_PATH):
            shutil.rmtree(NEW_FUNCTION_PATH)

        if os.path.exists(COMPOSITE_FUNCTION_PATH):
            shutil.rmtree(COMPOSITE_FUNCTION_PATH)

        if os.path.exists(RENAME_FUNCTION_PATH):
            shutil.rmtree(RENAME_FUNCTION_PATH)

        if os.path.exists(NOT_A_FUNCTION_PATH):
            shutil.rmtree(NOT_A_FUNCTION_PATH)

        if os.path.exists(MOVE_ON_TOP_FUNCTION_PATH):
            shutil.rmtree(MOVE_ON_TOP_FUNCTION_PATH) 

        # if os.path.exists(DELETE_FUNCTION_PATH):
        #     shutil.rmtree(DELETE_FUNCTION_PATH)

        # if os.path.exists(NEW_FUNCTION_RANG_PATH):
        #     shutil.rmtree(NEW_FUNCTION_RANG_PATH)

    def test_read_schema(self):
        schema = filemanager.FileManager._read_schema(REPO_PATH)
        self.assertEqual(schema, REPO_SCHEMA)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager._read_schema(WRONG_REPO_PATH)

        with self.assertRaises(NotADirectoryError):
            filemanager.FileManager._read_schema(FUNCTION_FILE_PATH)

    def test_is_function(self):
        is_function = filemanager.FileManager._is_function(REPO_PATH, FUNCTION_NAME)
        self.assertEqual(is_function, True)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager._is_function(WRONG_REPO_PATH, FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager._is_function(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(NotADirectoryError):
            filemanager.FileManager._is_function(REPO_PATH, NOT_FUNCTION_FILE_NAME)

    def test_create_repository(self):
        filemanager.FileManager.create_repository(NEW_REPO_PATH)

        schema = filemanager.FileManager._read_schema(NEW_REPO_PATH)

        self.assertEqual(os.path.exists(NEW_REPO_PATH), True)

        self.assertEqual(schema, NEW_REPO_SCHEMA)

        with self.assertRaises(FileExistsError):
            filemanager.FileManager.create_repository(REPO_PATH)

    def test_rename_repository(self):
        filemanager.FileManager.create_repository(NEW_REPO_PATH)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.rename_repository(WRONG_REPO_PATH, RENAME_REPO_NAME)

        with self.assertRaises(FileExistsError):
            filemanager.FileManager.rename_repository(NEW_REPO_PATH, REPO_NAME)
        
        filemanager.FileManager.rename_repository(NEW_REPO_PATH, RENAME_REPO_NAME)

        self.assertEqual(os.path.exists(RENAME_REPO_PATH), True)

    def test_delete_repository(self):
        filemanager.FileManager.create_repository(NEW_REPO_PATH)

        self.assertEqual(os.path.exists(NEW_REPO_PATH), True)

        filemanager.FileManager.delete_repository(NEW_REPO_PATH)

        self.assertEqual(os.path.exists(NEW_REPO_PATH), False)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.delete_repository(WRONG_REPO_PATH)

    def test_create_function(self):
        schema = filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        self.assertEqual(os.path.exists(NEW_FUNCTION_PATH), True)

        filemanager.FileManager.create_function(REPO_PATH, COMPOSITE_FUNCTION_NAME)

        self.assertEqual(os.path.exists(COMPOSITE_FUNCTION_PATH), True)

        self.assertEqual(schema, NEW_FUNCTION_SCHEMA)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.create_function(WRONG_REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileExistsError):
            filemanager.FileManager.create_function(REPO_PATH,FUNCTION_NAME)

    def test_all_children_functions(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.all_children_functions(REPO_PATH, NEW_FUNCTION_NAME)

        schema = filemanager.FileManager.all_children_functions(REPO_PATH)

        self.assertEqual(schema, REPO_SCHEMA)     

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, MOVE_ON_TOP_FUNCTION_NAME)

        schema = filemanager.FileManager.all_children_functions(REPO_PATH, NEW_FUNCTION_NAME)

        self.assertEqual(schema, ALL_CHILDREN_FUNCTIONS_SCHEMA)

    def test_rename_function(self):
        # test composite path function name
        # test rename function tree
        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        filemanager.FileManager.rename_function(REPO_PATH, NEW_FUNCTION_NAME, RENAME_FUNCTION_NAME)

        self.assertEqual(os.path.exists(RENAME_FUNCTION_PATH), True)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.rename_function(WRONG_REPO_PATH, NEW_FUNCTION_NAME, RENAME_FUNCTION_NAME)
               
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.rename_function(REPO_PATH, NEW_FUNCTION_NAME, RENAME_FUNCTION_NAME)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.rename_function(REPO_PATH, NOT_A_FUNCTION_NAME, RENAME_FUNCTION_NAME)

    def test_move_function(self):
        # test composite function path
        # test move function tree
        os.mkdir(NOT_A_FUNCTION_PATH)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.move_function(WRONG_REPO_PATH, NEW_FUNCTION_NAME, MOVE_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.move_function(REPO_PATH, NEW_FUNCTION_NAME, MOVE_FUNCTION_NAME)

        with self.assertRaises(filemanager.NotAFunctionError):
            filemanager.FileManager.move_function(REPO_PATH, NOT_A_FUNCTION_NAME, MOVE_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.move_function(REPO_PATH, NEW_FUNCTION_NAME, MOVE_FUNCTION_NAME)

        with self.assertRaises(filemanager.NotAFunctionError):
            filemanager.FileManager.move_function(REPO_PATH, NEW_FUNCTION_NAME, NOT_A_FUNCTION_NAME)

        with self.assertRaises(filemanager.WorngFunctionNameError):
            filemanager.FileManager.move_function(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_NAME)

        filemanager.FileManager.rename_function(REPO_PATH, NEW_FUNCTION_NAME, MOVE_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        filemanager.FileManager.move_function(REPO_PATH, MOVE_FUNCTION_NAME, NEW_FUNCTION_NAME)

        self.assertEqual(os.path.exists(MOVE_FUNCTION_PATH), True)

    def test_move_function_on_top(self):
        # test move function tree
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.move_function_on_top(WRONG_REPO_PATH, MOVE_ON_TOP_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.move_function_on_top(REPO_PATH, MOVE_ON_TOP_FUNCTION_NAME)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.move_function_on_top(REPO_PATH, NOT_A_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        filemanager.FileManager.rename_function(REPO_PATH, NEW_FUNCTION_NAME, MOVE_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        filemanager.FileManager.move_function(REPO_PATH, MOVE_FUNCTION_NAME, NEW_FUNCTION_NAME)

        filemanager.FileManager.move_function_on_top(REPO_PATH, MOVE_ON_TOP_FUNCTION_NAME)

        self.assertEqual(os.path.exists(MOVE_ON_TOP_FUNCTION_PATH), True)

    def test_delete_function(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.delete_function(WRONG_REPO_PATH, DELETE_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.delete_function(REPO_PATH, DELETE_FUNCTION_NAME)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.delete_function(REPO_PATH, NOT_A_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, DELETE_FUNCTION_NAME)

        self.assertEqual(os.path.exists(DELETE_FUNCTION_PATH), True)

        filemanager.FileManager.delete_function(REPO_PATH, DELETE_FUNCTION_NAME)

        self.assertEqual(os.path.exists(DELETE_FUNCTION_PATH), False)

    def test_create_function_rang(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.create_function_rang(WRONG_REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.create_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.create_function_rang(REPO_PATH, NOT_A_FUNCTION_NAME, NEW_FUNCTION_RANG)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileExistsError):
            filemanager.FileManager.create_function_rang(REPO_PATH, NEW_FUNCTION_NAME, DEFAULT_FUNCTION_RANG)

        filemanager.FileManager.create_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        self.assertEqual(os.path.exists(NEW_FUNCTION_RANG_PATH), True)

    def test_set_default_fuction_rang(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.set_default_fuction_rang(WRONG_REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.set_default_fuction_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.set_default_fuction_rang(REPO_PATH, NOT_A_FUNCTION_NAME, NEW_FUNCTION_RANG)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.set_default_fuction_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        filemanager.FileManager.create_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG, NEW_RANG_DEFAULT_VALUE)

        filemanager.FileManager.set_default_fuction_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with open(NEW_RANG_FUNCTION_FILE_PATH) as file:
            text = file.read()
            self.assertEqual(text, NEW_RANG_DEFAULT_VALUE)

    def test_delete_function_rang(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.delete_function_rang(WRONG_REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.delete_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.delete_function_rang(REPO_PATH, NOT_A_FUNCTION_NAME, NEW_FUNCTION_RANG)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.delete_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        filemanager.FileManager.create_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        self.assertEqual(os.path.exists(NEW_FUNCTION_RANG_PATH), True)

        filemanager.FileManager.delete_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)        

        self.assertEqual(os.path.exists(NEW_FUNCTION_RANG_PATH), False)

    def test_all_function_rangs(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.all_function_rangs(WRONG_REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.all_function_rangs(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.all_function_rangs(REPO_PATH, NOT_A_FUNCTION_NAME, NEW_FUNCTION_RANG)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        filemanager.FileManager.create_function_rang(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        schema = filemanager.FileManager.all_function_rangs(REPO_PATH, NEW_FUNCTION_NAME)

        self.assertEqual(schema, ALL_FUNCTION_RANGS_SCHEMA)

    def test_get_file_path(self):
        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.get_file_path(WRONG_REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.get_file_path(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(filemanager.NotAFunctionError):
            os.mkdir(NOT_A_FUNCTION_PATH)
            filemanager.FileManager.get_file_path(REPO_PATH, NOT_A_FUNCTION_NAME)

        filemanager.FileManager.create_function(REPO_PATH, NEW_FUNCTION_NAME)

        with self.assertRaises(FileNotFoundError):
            filemanager.FileManager.get_file_path(REPO_PATH, NEW_FUNCTION_NAME, NEW_FUNCTION_RANG)

        path = filemanager.FileManager.get_file_path(REPO_PATH, NEW_FUNCTION_NAME)

        self.assertEqual(path, GET_FILE_PATH_FILE_PATH)

        self.assertEqual(os.path.exists(path), True)

if __name__ == '__main__':
    unittest.main()
