import re

import os

import time

import shutil

from collections import OrderedDict

# CONFIGURATION CONSTANTS

DEFAULT_SCHEMA_FILE = 'schema.json'

DEFAULT_FUNCTION_FILE_NAME = '1.pfg'

DEFAULT_RANG = '00.00'

DEFAULT_VALUE = str()

RANG_PATTERN = r'(?P<path>.+/\w+/(?P<rang>\d{2}.\d{2}))'

FUNCTION_PATTERN = r'(?P<path>.+/(?P<name>\w+))/\d{2}.\d{2}/%s' % DEFAULT_FUNCTION_FILE_NAME

FUNCTION_TEMPLATE = os.path.join('{parent}', '{name}')

RANG_TEMPLATE = os.path.join(FUNCTION_TEMPLATE, '{rang}')

FILE_TEMPLATE = os.path.join(RANG_TEMPLATE, DEFAULT_FUNCTION_FILE_NAME)

# ERROR TEXT CONSTANTS

PATH_NOT_EXISTS_ERROR_TEXT = 'Path "%s" is not exist'

REPOSITORY_NOT_EXISTS_ERROR_TEXT = 'Repository with path "%s" is not exist'

REPOSITORY_EXISTS_ERROR_TEXT = 'Repository with path "%s" already exists.'

REPOSITORY_NAME_EXISTS_ERROR_TEXT = 'Repository with name "%s" already exists.'

NOT_A_DIRECTORY_ERROR_TEXT = 'Path "%s" should contains a directory.'

NOT_A_FUNCTION_ERROR_TEXT = 'Directory with path "%s" is not a function'

FUNCTION_NOT_EXISTS_ERROR_TEXT = 'Function with name "%s" is not exist'

FUNCTION_EXISTS_ERROR_TEXT = 'Function with name "%s" already exists.'

IT_SELF_ERROR_TEXT = 'Can not move function "%s" inside it self.'

RANG_EXIST_ERROR_TEXT = 'Function "%s" with rang "%s" already exists.'

RANG_NOT_EXIST_ERROR_TEXT = 'Function "%s" with rang "%s" not exists.'

class NotAFunctionError(Exception):
    pass

class WorngFunctionNameError(Exception):
    pass

def require_function_exist_mixin(function):
    def wraper(repository_path, full_name, *args, **kwargs):
        if not os.path.exists(repository_path):
            raise FileNotFoundError(REPOSITORY_NOT_EXISTS_ERROR_TEXT % repository_path)

        func_repo_path = full_name.replace('.', '/')
        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        if not os.path.exists(func_full_path):
            raise FileNotFoundError(FUNCTION_NOT_EXISTS_ERROR_TEXT % full_name)

        if not FileManager._is_function(repository_path, full_name):
            raise NotAFunctionError(NOT_A_FUNCTION_ERROR_TEXT % func_full_path)

        return function(repository_path, full_name, *args, **kwargs)

    return wraper

class FileManager(object):

    @staticmethod
    def _read_schema(path):
        if not os.path.exists(path):
            raise FileNotFoundError(PATH_NOT_EXISTS_ERROR_TEXT % path)

        if not os.path.isdir(path):
            raise NotADirectoryError(NOT_A_DIRECTORY_ERROR_TEXT % path)

        func_name = os.path.basename(path)

        folders = filter(lambda f: os.path.isdir(os.path.join(path, f)), os.listdir(path))

        rangs = map(lambda f: RANG_TEMPLATE.format(parent=path, name=f, rang=DEFAULT_RANG), folders)

        funcs = map(lambda f: os.path.join(f, DEFAULT_FUNCTION_FILE_NAME), rangs)

        files = filter(lambda f: os.path.exists(f), funcs)

        matches = re.findall(FUNCTION_PATTERN, '\n'.join(files))

        children = [OrderedDict([('name', name), ('path', path)]) for path, name in matches]

        return OrderedDict([('name', func_name), ('path', path), ('children', children)])

    @staticmethod
    def _is_function(repository_path, full_name, rang=DEFAULT_RANG):
        if not os.path.exists(repository_path):
            raise FileNotFoundError(REPOSITORY_NOT_EXISTS_ERROR_TEXT % repository_path)

        func_repo_path = full_name.replace('.', '/')

        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        if not os.path.exists(func_full_path):
            raise FileNotFoundError(FUNCTION_NOT_EXISTS_ERROR_TEXT % full_name)

        if not os.path.isdir(func_full_path):
            raise NotADirectoryError(NOT_A_DIRECTORY_ERROR_TEXT % func_full_path)

        rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=rang)

        file_full_path = os.path.join(rang_full_path, DEFAULT_FUNCTION_FILE_NAME)

        return os.path.exists(file_full_path)

    @staticmethod
    def create_repository(path):
        if os.path.exists(path):
            raise FileExistsError(REPOSITORY_EXISTS_ERROR_TEXT % path)

        os.mkdir(path)

        return FileManager._read_schema(path)

    @staticmethod
    def rename_repository(path, new_name):
        if not os.path.exists(path):
            raise FileNotFoundError(REPOSITORY_NOT_EXISTS_ERROR_TEXT % path)

        parent, name = os.path.split(path)

        new_path = os.path.join(parent, new_name)

        if os.path.exists(new_path):
            raise FileExistsError(REPOSITORY_NAME_EXISTS_ERROR_TEXT % new_name)

        os.rename(path, new_path)

    @staticmethod
    def delete_repository(path):
        if not os.path.exists(path):
            raise FileNotFoundError(REPOSITORY_NOT_EXISTS_ERROR_TEXT % path)

        shutil.rmtree(path)

    @staticmethod
    def create_function(repository_path, full_name, default=DEFAULT_VALUE):
        if not os.path.exists(repository_path):
            raise FileNotFoundError(REPOSITORY_NOT_EXISTS_ERROR_TEXT % repository_path)

        func_repo_path = full_name.replace('.', '/')
        rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=DEFAULT_RANG)

        if os.path.exists(rang_full_path):
            raise FileExistsError(FUNCTION_EXISTS_ERROR_TEXT % full_name)

        if not os.path.exists(rang_full_path):
            os.makedirs(rang_full_path)

        file_full_path = os.path.join(rang_full_path, DEFAULT_FUNCTION_FILE_NAME)

        with open(file_full_path, 'w') as file:
            file.write(default)

        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        return FileManager._read_schema(func_full_path)

    @staticmethod
    def all_children_functions(repository_path, full_name=None):
        parent_full_path = str()
        if not full_name:
            parent_full_path = repository_path
        else:
            func_repo_path = full_name.replace('.', '/')
            parent_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        if not os.path.exists(parent_full_path):
            raise FileNotFoundError('Function or repository with path "%s" not exists.' % parent_full_path)

        return FileManager._read_schema(parent_full_path)

    @staticmethod
    @require_function_exist_mixin
    def rename_function(repository_path, full_name, new_name):
        func_repo_path = full_name.replace('.', '/')
        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        new_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=new_name)

        os.rename(func_full_path, new_full_path)

    @staticmethod
    @require_function_exist_mixin
    def move_function(repository_path, full_name, parent_full_name):
        func_repo_path = full_name.replace('.', '/')
        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        parent_repo_path = parent_full_name.replace('.', '/')
        parent_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=parent_repo_path)

        if not os.path.exists(parent_full_path):
            raise FileNotFoundError(NOT_A_FUNCTION_ERROR_TEXT % parent_full_name)

        if not FileManager._is_function(repository_path, parent_full_name):
            raise NotAFunctionError(NOT_A_FUNCTION_ERROR_TEXT % parent_full_path)

        if full_name == parent_full_name:
            raise WorngFunctionNameError(IT_SELF_ERROR_TEXT % full_name)

        parent_path, name = os.path.split(full_name)

        new_full_path = os.path.join(parent_full_path, name)

        shutil.move(func_full_path, new_full_path)

    @staticmethod
    @require_function_exist_mixin
    def move_function_on_top(repository_path, full_name):
        func_repo_path = full_name.replace('.', '/')
        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        shutil.move(func_full_path, repository_path)

    @staticmethod
    @require_function_exist_mixin
    def delete_function(repository_path, full_name):
        func_repo_path = full_name.replace('.', '/')
        func_full_path = FUNCTION_TEMPLATE.format(parent=repository_path, name=func_repo_path)

        shutil.rmtree(func_full_path)

    @staticmethod
    @require_function_exist_mixin
    def create_function_rang(repository_path, full_name, rang, default=DEFAULT_VALUE):
        func_repo_path = full_name.replace('.', '/')

        rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=rang)

        if os.path.exists(rang_full_path):
            raise FileExistsError(RANG_EXIST_ERROR_TEXT % (full_name, rang))

        if not os.path.exists(rang_full_path):
            os.makedirs(rang_full_path)

        file_full_path = os.path.join(rang_full_path, DEFAULT_FUNCTION_FILE_NAME)

        with open(file_full_path, 'w') as file:
            file.write(default)

    @staticmethod
    @require_function_exist_mixin
    def set_default_fuction_rang(repository_path, full_name, rang):
        func_repo_path = full_name.replace('.', '/')

        new_rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=rang)

        if not os.path.exists(new_rang_full_path):
            raise FileNotFoundError(RANG_NOT_EXIST_ERROR_TEXT % (full_name, rang))

        default_rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=DEFAULT_RANG)

        variable_rang_name = str(time.time())
        variable_full_rang_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=variable_rang_name)

        os.rename(default_rang_full_path, variable_full_rang_path)

        os.rename(new_rang_full_path, default_rang_full_path)

        os.rename(variable_full_rang_path, new_rang_full_path)

    @staticmethod
    @require_function_exist_mixin
    def delete_function_rang(repository_path, full_name, rang):
        func_repo_path = full_name.replace('.', '/')

        rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=rang)

        if not os.path.exists(rang_full_path):
            raise FileNotFoundError(RANG_NOT_EXIST_ERROR_TEXT % (full_name, rang))

        shutil.rmtree(rang_full_path)

    @staticmethod
    @require_function_exist_mixin
    def all_function_rangs(repository_path, full_name):
        func_repo_path = full_name.replace('.', '/')

        func_full_path = os.path.join(repository_path, func_repo_path)

        content = map(lambda f: os.path.join(func_full_path, f), os.listdir(func_full_path))

        folders = filter(lambda f: os.path.isdir(f), content)

        matches = re.findall(RANG_PATTERN, '\n'.join(folders))

        rangs = [OrderedDict([('rang', rang), ('path', path)]) for path, rang in matches]

        return OrderedDict([('name', full_name), ('path', func_full_path), ('rangs', rangs)])

    @staticmethod
    @require_function_exist_mixin
    def get_file_path(repository_path, full_name, rang=DEFAULT_RANG):
        func_repo_path = full_name.replace('.', '/')

        rang_full_path = RANG_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=rang)

        if not os.path.exists(rang_full_path):
            raise FileNotFoundError(RANG_NOT_EXIST_ERROR_TEXT % (full_name, rang))

        return FILE_TEMPLATE.format(parent=repository_path, name=func_repo_path, rang=rang)
