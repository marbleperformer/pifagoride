import os

import re

import json

import shutil

from configuration.configuration import ConfigMetaclass, ConfigAttribute


class ValidationError(Exception):

    message_text = 'Function name and file name does not match.'

    def __init__(self):

        super(ValidationError, self).__init__(self.message_text)


class RepositoryConfig(metaclass=ConfigMetaclass):

    suffix = ConfigAttribute('suffix', str, '')

    prefix = ConfigAttribute('prefix', str, 'pfg')

    function_format = ConfigAttribute('function_format', str, '%(repository)s/pfg/%(functionname)s')

    rig_format = ConfigAttribute('rig_format', str, '%(repository)s/pfg/%(functionname)s')

    cg_format = ConfigAttribute('cg_format', str, '%(repository)s/pfg/%(functionname)s')

    content_format = ConfigAttribute('content_format', str, '%(name)s << funcdef %(argument)s {%(body)s}')

    validation_template = ConfigAttribute('validation_template', str, r'(?P<name>[\w|\.]+)\s*<<\s*funcdef\s*(?P<argument>\w*)\s*{(?P<body>(.|\s)*)}')

    function_template = ConfigAttribute('function_template', str, r'pfg/(?P<functionpath>.+)')

    default_argument = ConfigAttribute('default_argument', str, 'arg')

    default_body = ConfigAttribute('default_argument', str, '\n \n')


    def __init__(self, path=None):

        if path:

            with open(path, 'r') as file:

                config_json = json.load(file)

                for name, value in config_json.items():

                    setattr(self, name, value)


class Repository(object):

    def __init__(self, path):

        self._path = path

        self._config = RepositoryConfig()


    @property
    def functions(self):

        for dirpath, dirnames, filenames in os.walk(self.path):

            for name in filenames:

                sfx = self._config.suffix

                pfx = self._config.prefix

                if name.endswith(sfx) and dirpath.endswith(pfx):

                    full_path = os.path.join(dirpath, name)

                    file_path = full_path.replace(self.path, '').strip(os.sep)

                    match = re.match(self._config.function_template, file_path)

                    if match:

                        groups = match.groupdict()

                        func_path = groups.get('functionpath')

                        yield func_path.replace(os.sep, '.')


    @property
    def path(self):

        path = self._path.replace('/', os.sep)

        path = self._path.replace('\\', os.sep)

        return path


    def _get_function_path(self, name):

        function_path = name.replace('.', os.sep)

        # print(self._config.function_format % {'repository':self._path, 'functionname':name, 'functionpath':function_path})

        return self._config.function_format % {'repository':self.path, 'functionname':name, 'functionpath':function_path}


    def _get_rig_path(self, name):

        function_path = name.replace('.', os.sep)

        return self._config.rig_format % {'repository':self.path, 'functionname':name, 'functionpath':function_path}


    def _get_cg_path(self, name):

        function_path = name.replace('.', os.sep)

        return self._config.cg_format % {'repository':self.path, 'functionname':name, 'functionpath':function_path}


    def _get_content_components(self, content):

        # print(content)

        # print(self._config.validation_template)

        match = re.match(self._config.validation_template, content)

        # print(match)

        groups = match.groupdict()

        name = groups.get('name')

        body = groups.get('body')

        argument = groups.get('argument')

        return name, argument, body


    def create_function(self, name):

        path = self._get_function_path(name)

        default_body = self._config.default_body

        default_argument = self._config.default_argument

        content = self._config.content_format % {'name':name, 'argument':default_argument, 'body':default_body}

        with open(path, 'w') as file:

            file.write(content)


    def rename_function(self, name, rename):

        path = self._get_function_path(name)

        new_path = self._get_function_path(rename)

        os.rename(path, new_path)

        with open(new_path, 'r') as file:

            content = file.read()

            name, argument, body = self._get_content_components(content)

            new_content = self._config.content_format % {'name':rename, 'argument':argument, 'body':body}

        with open(new_path, 'w') as file:

            file.write(new_content)
            

    def update_function(self, name, content):

        path = self._get_function_path(name)

        # function_path = 

        # path = self._config.function_format % {'repository':self._path, 'functionname':name}

        with open(path, 'r') as file:

            old_content = file.read()

            old_name, argument, body = self._get_content_components(old_content)

        new_name, argument, body = self._get_content_components(content)
        
        if new_name != old_name:

            raise ValidationError

        with open(path, 'w+') as file:
            
            file.write(content)


    def write_function(self, name, content):

        path = self._get_function_path(name)

        with open(path, 'w') as file:

            file.write(content)


    def remove_function(self, name):

        path = self._get_function_path(name)

        os.remove(path)


    def get_function_content(self, name):

        path = self._get_function_path(name)

        with open(path, 'r') as file:

            return file.read()


class RepositoryManager(object):

    @staticmethod
    def create_repository(path):       

        os.mkdir(path)


    @staticmethod
    def init_repository(path):

        if not os.path.isdir(path):

            raise FileNotFoundError("No such file or directory: '%s'" % path)

        return Repository(path)


    @staticmethod
    def remove_repository(path):

        shutil.rmtree(path)