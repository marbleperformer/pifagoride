import os

import shutil

import pytest

import importlib

from appcore.repositorymanager.repositorymanager import Repository, ValidationError


@pytest.fixture
def path():

    return 'repository'


@pytest.fixture
def repository(path):

    return Repository(path)


@pytest.fixture
def function_name():

    return 'testfunction'


@pytest.fixture
def function_fullpath(path, function_name):

    return os.path.join(path, 'pfg', function_name)

@pytest.fixture
def content_template():

    return '%(name)s << funcdef %(argument)s {%(body)s}'


@pytest.fixture
def function_argument():

    return 'arg'


@pytest.fixture
def function_body():

    return '\n \n'


@pytest.fixture
def function_content(content_template, function_name, function_argument, function_body):

    return content_template % {'name':function_name, 'argument':function_argument, 'body':function_body}


@pytest.fixture
def created_function_required(function_fullpath, function_content):

    with open(function_fullpath, 'w') as file:

        file.write(function_content)


@pytest.fixture
def rename_function_name():

    return 'newfunction'


@pytest.fixture
def rename_function_fullpath(path, rename_function_name):

    return os.path.join(path, 'pfg', rename_function_name)


@pytest.fixture
def rename_function_content(content_template, rename_function_name, function_argument, function_body):

    return content_template % {'name':rename_function_name, 'argument':function_argument, 'body':function_body}


@pytest.fixture
def update_fucntion_body():

    return 'return << 123'


@pytest.fixture
def update_function_content(content_template, function_name, function_argument, update_fucntion_body):

    return content_template % {'name':function_name, 'argument':function_argument, 'body':update_fucntion_body}


@pytest.fixture
def wrong_funtion_name():

    return 'wrongfunction'


@pytest.fixture
def wrong_function_content(content_template, wrong_funtion_name, function_argument, update_fucntion_body):

    return content_template % {'name':wrong_funtion_name, 'argument':function_argument, 'body':update_fucntion_body}


def setup_function(function):

    repository_names = [
        'repository', 
        'repository/pfg', 
        'repository/rig', 
        'repository/cg'
    ]

    function_names = [
        'repository/pfg/func1',
        'repository/pfg/func2',
        'repository/pfg/func3'
    ]

    for name in repository_names:

        os.mkdir(name)

    for name in function_names:

        with open(name, 'w'):

            pass


def teardown_function(function):

    shutil.rmtree('repository')


def test_functions(repository):

    funcs = list(repository.functions)

    assert len(funcs) == 3


def test_path(repository, path):

    assert repository.path == path
    

def test_name_create_function(repository, function_name, function_fullpath):

    repository.create_function(function_name)

    assert os.path.exists(function_fullpath)


def test_content_create_function(repository, function_name, function_fullpath, function_content):

    repository.create_function(function_name)

    with open(function_fullpath, 'r') as file:

        content = file.read()

        assert content == function_content


def test_name_rename_function(repository, created_function_required, function_name, rename_function_name, rename_function_fullpath):

    repository.rename_function(function_name, rename_function_name)

    assert os.path.exists(rename_function_fullpath)


def test_content_rename_function(repository, created_function_required, function_name, rename_function_name, rename_function_fullpath, rename_function_content):

    repository.rename_function(function_name, rename_function_name)

    with open(rename_function_fullpath) as file:

        content = file.read()

        assert content == rename_function_content


def test_update_function(repository, created_function_required, function_name, function_fullpath, update_function_content):

    repository.update_function(function_name, update_function_content)

    with open(function_fullpath, 'r') as file:

        content = file.read()

        assert content == update_function_content


@pytest.mark.xfail(raises=ValidationError)
def test_wrongname_update_function(repository, created_function_required, function_name, wrong_function_content):

    repository.update_function(function_name, wrong_function_content)


def test_remove_function(repository, created_function_required, function_name, function_fullpath):

    repository.remove_function(function_name)

    assert not os.path.exists(function_fullpath) 


def test_get_function_content(repository, created_function_required, function_name, function_content):

    content = repository.get_function_content(function_name)

    assert content == function_content
