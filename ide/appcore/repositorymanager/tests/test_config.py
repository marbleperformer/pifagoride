import pytest

import os

import json

from appcore.repositorymanager.repositorymanager import RepositoryConfig


@pytest.fixture
def sign():

    return 'test'


@pytest.fixture
def name_template():

    return '%(repository)s/test/%(function)s.test'


@pytest.fixture
def content_template():

    return '%(function)s << funcdef {\n \n}'


@pytest.fixture
def config_required(sign, name_template, content_template):

    with open('testrepository.conf', 'w') as file:

        json.dump({
            'sign':sign, 
            'name_template':name_template, 
            'content_template':content_template
        }, file)

    return RepositoryConfig('testrepository.conf')


def teardown_function(function):

    RepositoryConfig._ConfigMetaclass__instance = None

    if os.path.exists('testrepository.conf'):

        os.remove('testrepository.conf')


def test_sign(config_required, sign):

    assert config_required.sign == sign


def test_name_template(config_required, name_template):

    assert config_required.name_template == name_template


def test_content_template(config_required, content_template):

    assert config_required.content_template == content_template
