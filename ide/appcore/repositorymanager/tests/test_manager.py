import os

import shutil

import pytest

from appcore.repositorymanager.repositorymanager import RepositoryManager, Repository


@pytest.fixture
def repository_name():

    return 'repository'


@pytest.fixture
def repository_fullpath(repository_name):

    return repository_name


@pytest.fixture
def created_repository_required(repository_fullpath):

    os.mkdir(repository_fullpath)


def teardown_function(function):

    if os.path.exists('repository'):

        shutil.rmtree('repository')


def test_create_repository(repository_fullpath):

    RepositoryManager.create_repository(repository_fullpath)

    assert os.path.isdir(repository_fullpath)


@pytest.mark.xfail(raises=FileExistsError)
def test_exists_create_repository(repository_fullpath, created_repository_required):

    RepositoryManager.create_repository(repository_fullpath)


def test_init_repository(repository_fullpath, created_repository_required):

    repository = RepositoryManager.init_repository(repository_fullpath)

    assert isinstance(repository, Repository)


@pytest.mark.xfail(raises=FileNotFoundError)
def test_notexists_init_repository(repository_fullpath):

    RepositoryManager.init_repository(repository_fullpath)


def test_remove_repository(repository_fullpath, created_repository_required):

    RepositoryManager.remove_repository(repository_fullpath)

    assert not os.path.exists(repository_fullpath)


@pytest.mark.xfail(raises=FileNotFoundError)
def test_notexists_remove_repository(repository_fullpath):

    RepositoryManager.remove_repository(repository_fullpath)
