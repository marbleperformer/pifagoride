import os
import sys
import json

import inspect

import unittest

from collections import OrderedDict

from PyQt5.QtWidgets import QApplication

PLUGINS_NAME = 'plugins'

GUICORE_NAME = 'guicore'

APPCORE_NAME = 'appcore'

TEST_FILE_NAME = 'tests.py'

TEST_MODULE_NAME = 'tests'

GUI_FILE_NAME = 'gui.py'

GUI_MODULE_NAME = 'gui'

PLUGINS_VAR_NAME = 'PLUGINS'

CONFIG_PATH = 'main.conf'

if __name__ == '__main__':
    def test_guicore():
        suite = unittest.TestSuite()

        guicore_path = os.path.join(os.getcwd(), GUICORE_NAME)

        module = __import__(GUICORE_NAME)

        for name in os.listdir(guicore_path):
            test_full_path = os.path.join(guicore_path, name, TEST_FILE_NAME)

            if os.path.exists(test_full_path):
                
                module_full_name = '.'.join((GUICORE_NAME, name, TEST_MODULE_NAME))

                guicore_module = __import__(module_full_name)

                module = getattr(guicore_module, name)
                module = getattr(module, TEST_MODULE_NAME) 
        
                cases = unittest.findTestCases(module)

                suite.addTest(cases)

        return suite

    def gui_guicore(module_name):

        gui_full_path = os.path.join(os.getcwd(), GUICORE_NAME, module_name, GUI_FILE_NAME)

        if os.path.exists(gui_full_path):

            module_full_name = '.'.join((GUICORE_NAME, module_name, GUI_MODULE_NAME))

            guicore_module = __import__(module_full_name)

            module = getattr(guicore_module, module_name)
            module = getattr(module, GUI_MODULE_NAME)

            app = QApplication(sys.argv)

            module._show_(app)

            sys.exit(app.exec_())

    if len(sys.argv) >= 2:
        if sys.argv[1] == 'test':

            sys.argv.pop()

            unittest.main(defaultTest='test_guicore')

        elif sys.argv[1] == 'gui':

            gui_guicore(sys.argv[2])

            sys.argv = sys.argv[:1]

    else:
        app = QApplication(sys.argv)

        config_full_path = os.path.join(os.getcwd(), CONFIG_PATH)

        with open(config_full_path) as file:
            
            config = json.load(file, object_pairs_hook=OrderedDict)

            plugins = config[PLUGINS_VAR_NAME]

            for name in plugins:
                plugin_full_name = '.'.join((PLUGINS_NAME, name))

                module = __import__(plugin_full_name, {'application':app})

                plugin = module.__getattribute__(name)
                plugin._init_(app)

        sys.exit(app.exec_())
