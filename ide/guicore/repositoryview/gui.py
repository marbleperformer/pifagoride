from .repositoryview import TreeView

def _show_(app):

    app.view = TreeView()

    app.view.show()
