import os, sys

from collections import OrderedDict

from PyQt5.QtWidgets import QTreeView, QAbstractItemView, QMenu, QFileDialog

from PyQt5.QtGui import QStandardItemModel, QCursor

from PyQt5.QtCore import QSortFilterProxyModel, Qt, QPoint, QModelIndex, pyqtSlot

from guicore.repositorydialogs import RepositoryCreateDialog

from guicore.functiondialogs import FunctionCreateDialog, FunctionDeleteDialog

from guicore.repositoryitems.repositoryitems import RepositoryItem

from guicore.actionmanager.actionmanager import StaticActionManagerMixin

# from guicore.functionitems.functionitems import FunctionItem

from guicore.itemmanager.itemmanager import ItemManagerMixin

# from guicore.observer.observer import Actor

# from .treeitems import FunctionItem, RepositoryItem

# from treeitems import FunctionItem, RepositoryItem

# from fileserializer import FileSerializer

class TreeView(ItemManagerMixin, QTreeView):

    def __new__(cls, *args, **kwargs):

        super().__new__(cls)

class TreeView(ItemManagerMixin, QTreeView):

    def __init__(self, *args, **kwargs):

        super(TreeView, self).__init__(*args, **kwargs)

        self._menu = QMenu()
        
        self._selected_item = None

        self._selected_repo = None

        self._repositories = list()

        self._source_model = QStandardItemModel()

        self._proxy_model = QSortFilterProxyModel()

        self._proxy_model.setSourceModel(self._source_model)

        self.setContextMenuPolicy(Qt.CustomContextMenu)

        self.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.clicked.connect(self.set_selected_item)

        self.clicked.connect(self.item_clicked)

        self.customContextMenuRequested.connect(self.set_selected_item)

        self.customContextMenuRequested.connect(self.set_point_selected_item)

        self.setModel(self._proxy_model)


    def preform_init(self):
        pass


    def init(self):

        # TODO open dialog

        # TODO if dialog accept create repository

        # TODO notificate about init

        pass


    def open(self):

        path = QFileDialog.getExistingDirectory(self, 'Open repository')

        if path:

            name = os.basedir(path)

            item = RepositoryItem(name, path)

            self._source_model.appendRow(item)



            return item 

        # TODO open dialog

        # TODO if dialog accept create repository item

        # TODO notificate about init

        pass


    def close(self):

        # TODO close repository

        # TODO notificate about close

        pass


    def create(self):

        # TODO open dialog

        # TODO if dialog accept create function item

        # TODO notificate about create

        pass


    def delete(self):

        # TODO open dialog

        # TODO if dialog accept delete function item

        # TODO notificate about delete

        pass


    def select_repo(self, repository):

        # TODO select repository

        # TODO notificate about select repo

        pass


    def select_function(self, repository, full_name, rang):

        # TODO select function

        # TODO notificate about select

        pass


    def init(self, serializer):
        self.preform_init()
        repo_obj = serializer.repository
        if repo_obj['url'] in self.repositories:
            raise Exception('Repository with that url already opened.')
        repo_item = RepositoryItem(serializer, url=repo_obj['url'], name=repo_obj['name'])
        self.source_model.appendRow(repo_item)
        self.repositories.append(repo_item.identity)

        for child_obj in repo_obj['children']:
            func_item = FunctionItem(repo_item, url=child_obj['url'], name=child_obj['name'], rang=child_obj['rang'], body=child_obj['body'])
            repo_item.add(func_item)

    def create(self, **attrs):
        if isinstance(self.selected_item, RepositoryItem):
            repo_item = self.selected_item
        else:
            repo_item = self.selected_item.repository
        item = FunctionItem(repo_item, **attrs)
        self.selected_item.add(item)

    def delete(self):
        current_item = self.selected_item
        if isinstance(self.selected_item, RepositoryItem):
            self.source_model.removeRow(self.selected_item.index().row())
        else:
            self.selected_item.parent.remove(self.selected_item)

    @pyqtSlot(str)
    def item_clicked(self, idx):
        item = self.selected_item
        if isinstance(item, FunctionItem):
            func_obj = item.repository.serializer.get(item.url)
            for child_obj in func_obj['children']:
                if child_obj['url'] not in item.children:
                    func_item = FunctionItem(item.repository, url=child_obj['url'], name=child_obj['name'], rang=child_obj['rang'], body=child_obj['body'])
                    item.add(func_item)

    @pyqtSlot(str)
    def set_selected_item(self, idx):
        if isinstance(idx, QModelIndex):
            sourse_index = self.proxy_model.mapToSource(idx)
            self.selected_item = self.source_model.itemFromIndex(sourse_index)
        elif idx is None:
            self.selected_item = None

    @pyqtSlot(str)
    def set_point_selected_item(self, point):
        if isinstance(point, QPoint):
            indexes = self.selectedIndexes()
            if indexes:
                proxy_index = indexes[0]
                sourse_index = self.proxy_model.mapToSource(proxy_index)
                self.selected_item = self.source_model.itemFromIndex(sourse_index)

# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     path = '/home/marble/repo'
#     view = TreeView()
#     view.show()
#     serializer = FileSerializer(path)
#     # j = OrderedDict([('url', '/home/marble/repo'), ('name', 'repo'), ('functions', ['/home/marble/repo/function21', '/home/marble/repo/function'])])
#     view.init(serializer)
#     # view.create(url=path, name='test', rang='00.00')
#     sys.exit(app.exec_())
