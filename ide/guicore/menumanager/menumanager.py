# from guicore.actions.actions import Menu

MENU_SIGN = '_menu'

STATIC_MUNU_SIGN = '_staticmenu'

INTERFACE_ERROR_TEXT = '"%s" should be overrided before use.'

WRONG_TYPE_ERROR_TEXT = 'Wrong type argument "%s", it should be "%s".'

MENU_NOT_EXITS_ERROR_TEXT = 'Menu "%s" not exist into "%s" context.'

NOT_MENU_AND_STRING_ERROR_TEXT = 'Wrong type argument "value", it should be "%s" or "%s".'

class MenuManagerMixin:

    @staticmethod
    def __supermenu__():

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__supermenu__')


    def __menubar__(self):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__menubar__')        


    def __target__(self):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__target__')        


    @property
    def menus(self):

        target = self.__target__()

        for field_name in target.__dict__:

            if field_name.endswith(MENU_SIGN):

                yield target.__dict__.get(field_name)


    def get_menu(self, name):

        target = self.__target__()

        menu_name = name + MENU_SIGN

        if not menu_name in target.__dict__:

            raise ValueError(MENU_NOT_EXITS_ERROR_TEXT % (name, target))

        return getattr(target, menu_name)


    def create_menu(self, title, icon_path=None):

        menu_class = self.__supermenu__()

        target = self.__target__()

        menu = menu_class(title, target, icon_path)

        menu_name = menu.name + MENU_SIGN

        setattr(self.__target__(), menu_name, menu)

        self.__menubar__().addMenu(menu)

        return menu


    def add_menu(self, menu):

        if not isinstance(menu, self.__supermenu__()):

            raise TypeError(WRONG_TYPE_ERROR_TEXT % ('menu', self.__supermenu__()))

        menu_name = menu.name + MENU_SIGN

        setattr(self.__target__(), menu_name, menu)

        self.__menubar__().addMenu(menu)

        return menu


    def remove_menu(self, value):

        target = self.__target__()

        menu_class = self.__supermenu__()

        if not isinstance(value, menu_class) and not isinstance(value, str):

            raise TypeError(NOT_MENU_AND_STRING_ERROR_TEXT % (menu_class, str))

        menu_name = (value.name if isinstance(value, menu_class) else value) + MENU_SIGN

        if not menu_name in target.__dict__:

            raise ValueError(MENU_NOT_EXITS_ERROR_TEXT % (value, target))

        menu_instance = value if isinstance(value, menu_class) else self.get_menu(value)

        self.__menubar__().removeAction(menu_instance.menuAction())

        delattr(target, menu_name)
        
        return menu_instance


class StaticMenuManagerMixin():

    @staticmethod
    def __staticsupermenu__():

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__staticsupermenu__')


    @classmethod
    def __staticmenubar__(cls):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__staticmenubar__')


    @classmethod
    def __statictarget__(cls):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__statictarget__')


    @classmethod
    def get_static_menus(cls):

        target = cls.__statictarget__()

        for field_name in target.__dict__:

            if field_name.endswith(STATIC_MUNU_SIGN):

                yield target.__dict__.get(field_name)


    @classmethod
    def get_static_menu(cls, name):

        target = cls.__statictarget__()

        menu_name = name + STATIC_MUNU_SIGN

        if not menu_name in target.__dict__:

            raise ValueError(MENU_NOT_EXITS_ERROR_TEXT % (name, target))

        return getattr(target, menu_name)


    @classmethod
    def add_static_menu(cls, menu):

        supermenu = cls.__staticsupermenu__()

        if not isinstance(menu, supermenu):

            raise TypeError(WRONG_TYPE_ERROR_TEXT % ('menu', supermenu))

        menu_name = menu.name + STATIC_MUNU_SIGN

        setattr(cls.__statictarget__(), menu_name, menu)

        cls.__staticmenubar__().addMenu(menu)


    @classmethod
    def remove_static_menu(cls, value):

        target = cls.__statictarget__()

        menu_class = cls.__staticsupermenu__()

        if not isinstance(value, str) and not isinstance(value, menu_class):

            raise TypeError(NOT_MENU_AND_STRING_ERROR_TEXT % (menu_class, str))

        menu_name = (value.name if isinstance(value, menu_class) else value) + STATIC_MUNU_SIGN

        if not menu_name in target.__dict__:

            raise ValueError(MENU_NOT_EXITS_ERROR_TEXT % (menu_name, target))

        menu_instance = (value if isinstance(value, menu_class) else cls.get_static_menu(value))

        cls.__staticmenubar__().removeAction(menu_instance.menuAction())

        delattr(target, menu_name)

        return menu_instance
