import unittest

from PyQt5.QtWidgets import QMainWindow, QMenu

from guicore.actions.actions import Menu

from .menumanager import MenuManagerMixin, StaticMenuManagerMixin


MENU_TITLE = 'Some menu'

MENU_NAME = 'some_menu'

WRONG_MENU_NAME = 'wrong_MENU_NAME'

MENU_STRING = 'menu'

ICON_PATH = 'static/Translate.png'

WRONG_REMOVE_VALUE = 123

STATIC_MENU_TITLE = 'static menu'


class DefaultMenuRequired:

    testcase = None


    def __init__(self, func):

        self._func = func


    def __call__(self, *args, **kwargs):

        global default_menu

        default_menu = self.testcase.manager.create_menu(MENU_TITLE, ICON_PATH)

        return self._func(self.testcase, *args, **kwargs)


class MenuManagerTestCase(unittest.TestCase):

    def setUp(self):

        class Manager(QMainWindow, MenuManagerMixin):
            @staticmethod
            def __supermenu__():
                return Menu

            def __menubar__(self):
                return self.menuBar()

            def __target__(self):
                return self

        self.manager = Manager()

        DefaultMenuRequired.testcase = self


    def test_not_implemented_supermenu(self):

        class Manager(MenuManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__supermenu__()


    def test_not_implemented_menubar(self):

        class Manager(MenuManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__menubar__()


    def test_not_implemented_target(self):

        class Manager(MenuManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__target__()


    def test_create_menu(self):

        menu = self.manager.create_menu(MENU_TITLE)
        
        attribute = self.manager.get_menu(MENU_NAME)

        self.assertIs(menu, attribute)


    def test_create_menu_with_icon(self):

        menu = self.manager.create_menu(MENU_TITLE, ICON_PATH)

        attribute = self.manager.get_menu(MENU_NAME)

        self.assertIs(menu, attribute)


    @DefaultMenuRequired
    def test_get_menu(self):

        menu = self.manager.get_menu(MENU_NAME)

        self.assertIsNotNone(menu)


    @DefaultMenuRequired
    def test_get_wrong_menu(self):

        with self.assertRaises(ValueError):

            self.manager.get_menu(WRONG_MENU_NAME)


    def test_add_menu(self):

        menu = Menu(MENU_TITLE, self.manager)

        self.manager.add_menu(menu)

        attribute = self.manager.get_menu(MENU_NAME)

        self.assertIsNotNone(attribute)


    def test_add_wrong_menu(self):

        with self.assertRaises(TypeError):

            self.manager.add_menu(MENU_STRING)


    @DefaultMenuRequired
    def test_remove_menu(self):

        self.manager.remove_menu(MENU_NAME)

        menus = self.manager.__menubar__().actions()

        self.assertEqual(len(menus), 0)


    @DefaultMenuRequired
    def test_remove_menu_by_menu(self):

        attribute = self.manager.get_menu(MENU_NAME)

        self.manager.remove_menu(value=attribute)

        menus = self.manager.__menubar__().actions()

        self.assertEqual(len(menus), 0)


    @DefaultMenuRequired
    def test_remove_wrong_menu(self):

        with self.assertRaises(TypeError):

            self.manager.remove_menu(WRONG_REMOVE_VALUE)

            
    @DefaultMenuRequired
    def test_remove_not_exist_menu(self):

        with self.assertRaises(ValueError):

            self.manager.remove_menu(WRONG_MENU_NAME)


    @DefaultMenuRequired
    def test_remove_not_exist_action_by_menu(self):

        action = Menu(WRONG_MENU_NAME, self.manager)

        with self.assertRaises(ValueError):

            self.manager.remove_menu(action)


class DefaultStaticMenuRequired:

    testcase = None

    def __init__(self, func):

        self._func = func


    def __call__(self, *args, **kwargs):

        global default_menu

        parent = QMainWindow()

        default_menu = Menu(MENU_TITLE, parent)

        self.testcase.manager.add_static_menu(default_menu)

        return self._func(self.testcase, *args, **kwargs)


class StaticMenuManagerTestCase(unittest.TestCase):

    def setUp(self):

        class Manager(StaticMenuManagerMixin, QMainWindow):

            menu = QMenu()

            @staticmethod
            def __staticsupermenu__():
                return Menu


            @classmethod
            def __staticmenubar__(cls):
                return cls.menu


            @classmethod
            def __statictarget__(cls):
                return cls

        self.manager = Manager

        DefaultStaticMenuRequired.testcase = self


    def test_not_implemented_supermenu(self):

        class Manager(StaticMenuManagerMixin):
            pass

        with self.assertRaises(NotImplementedError):

            Manager.__staticsupermenu__()


    def test_not_implemented_menubar(self):

        class Manager(StaticMenuManagerMixin):
            pass

        with self.assertRaises(NotImplementedError):

            Manager.__staticmenubar__()


    def test_not_implemented_target(self):

        class Manager(StaticMenuManagerMixin):
            pass

        with self.assertRaises(NotImplementedError):

            Manager.__statictarget__()


    @DefaultStaticMenuRequired
    def test_get_menus(self):

        length = len(tuple(self.manager.get_static_menus()))

        self.assertGreater(length, 0)


    @DefaultStaticMenuRequired
    def test_get_menu(self):

        menu = self.manager.get_static_menu(MENU_NAME)

        self.assertIsNotNone(menu)


    def test_get_wron_menu(self):

        with self.assertRaises(ValueError):

            self.manager.get_static_menu(WRONG_MENU_NAME)


    def test_add_static_menu(self):

        parent = QMainWindow()

        menu = Menu(MENU_NAME, parent)

        self.manager.add_static_menu(menu)

        attribute = self.manager.get_static_menu(MENU_NAME)


    def test_add_wrong_static_menu(self):

        with self.assertRaises(TypeError):

            self.manager.add_static_menu(MENU_STRING)


    @DefaultStaticMenuRequired
    def test_remove_static_menu(self):

        self.manager.remove_static_menu(MENU_NAME)

        menus = self.manager.__staticmenubar__().actions()

        self.assertEqual(len(menus), 0)


    @DefaultStaticMenuRequired
    def test_remove_static_menu_by_menu(self):

        menu = self.manager.get_static_menu(MENU_NAME)

        self.manager.remove_static_menu(value=menu)

        menus = self.manager.__staticmenubar__().actions()

        self.assertEqual(len(menus), 0)


    @DefaultStaticMenuRequired
    def test_remove_wrong_static_menu(self):

        with self.assertRaises(TypeError):

            self.manager.remove_static_menu(WRONG_REMOVE_VALUE)


    @DefaultStaticMenuRequired
    def test_remove_not_exist_static_menu(self):

        with self.assertRaises(ValueError):

            self.manager.remove_static_menu(WRONG_MENU_NAME)


    @DefaultStaticMenuRequired
    def test_remove_not_exist_static_menu_by_menu(self):

        parent = QMainWindow()

        menu = Menu(WRONG_MENU_NAME, parent)

        with self.assertRaises(ValueError):

            self.manager.remove_static_menu(menu)


    # @DefaultMenuRequired
    # def test_delete_menu(self):

    #     self.manager.delete_menu(MENU_NAME)

    #     with self.assertRaises(ValueError):

    #         self.manager.get_menu(MENU_NAME)
