import os

import unittest

from PyQt5.QtGui import QStandardItemModel

from .functionitems import FunctionItem

from guicore.repositoryitems.repositoryitems import RepositoryItem


ITEM_NAME = 'test_item'

ITEM_TITLE = 'test item'

ITEM_PATH = 'path/to/item/'

CHILD_PATH = os.path.join(ITEM_PATH, ITEM_NAME)

class FunctionItemTestCase(unittest.TestCase):

    def setUp(self):

        model = QStandardItemModel()

        item = RepositoryItem(ITEM_TITLE, ITEM_PATH, model)

        self.item = FunctionItem(ITEM_TITLE, ITEM_PATH, item)


    def test_name(self):

        self.assertEqual(self.item.name, ITEM_NAME)


    def test_fullname(self):

        self.assertEqual(self.item.fullname, ITEM_NAME)


    def test_identity(self):

        self.assertEqual(self.item.identity, CHILD_PATH)
