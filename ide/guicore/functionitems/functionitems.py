
import os

from PyQt5.QtWidgets import QMenu

from PyQt5.QtGui import QStandardItem

from guicore.actionmanager.actionmanager import StaticActionManagerMixin

from guicore.actions.actions import Action


class FunctionItem(StaticActionManagerMixin, QStandardItem):

    def __new__(cls, *args, **kwargs):

        cls._menu = QMenu()

        return super().__new__(cls)


    def __init__(self, title, parent):

        super(FunctionItem, self).__init__(title)

        self._parent = parent

        # self._path = path


    def __target__(self):

        return self


    @staticmethod
    def __staticsuperaction__():

        return Action


    @classmethod
    def __staticactionbar__(cls):

        return cls._menu


    @classmethod
    def __statictarget__(cls):

        return cls


    @property
    def name(self):

        return self.text().replace(' ', '_').lower()


    @property
    def fullname(self):

        if isinstance(self._parent, FunctionItem):

            return '.'.join((self.name, self._parent.fullname))

        return self.name

    # @property
    # def path(self):

    #     return self._path

    # @property
    # def identity(self):

    #     return os.path.join(self._parent.identity, self.name) 


class ListFunctionItem(StaticActionManagerMixin, QStandardItem):

    # def __new__(cls, *args, **kwargs):

    #     cls._menu = QMenu()

    #     print(cls)

    #     return super().__new__(cls)


    def __init__(self, title, parent):

        super(ListFunctionItem, self).__init__(title)

        self._parent = parent

        # self._path = path


    def __target__(self):

        return self


    @staticmethod
    def __staticsuperaction__():

        return Action


    @classmethod
    def __staticactionbar__(cls):

        if not '_menu' in cls.__dict__:

            cls._menu = QMenu()

        return cls._menu


    @classmethod
    def __statictarget__(cls):

        return cls


    @property
    def name(self):

        return self.text().replace(' ', '_').lower()


    @property
    def fullname(self):

        if isinstance(self._parent, FunctionItem):

            return '.'.join((self.name, self._parent.fullname))

        return self.name


    # @property
    # def path(self):

    #     return self._path

    # @property
    # def identity(self):

    #     return os.path.join(self._parent.identity, self.name)
