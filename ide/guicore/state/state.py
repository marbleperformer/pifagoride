from utility.singleton import Singleton

from PyQt5.QtCore import pyqtSignal


class FunstionsState(metaclass=Singleton):
    

    created = pyqtSignal(str, dict)

    perform_created = pyqtSignal(str, dict)

    renamed = pyqtSignal(str, dict)

    selected = pyqtSignal(str, dict)

    
    def select(self, repository, data):

        self.selected.emit(repository, data)


    def create(self, repository, data):

        self.created.emit(repository, data)


    def rename(self, repository, data):

        self.renamed.emit(repository, data)


    def open(self, repository, data):

        self.opened.emit(repository, data)
