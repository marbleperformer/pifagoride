import os
import sys

from PyQt5.QtWidgets import QApplication

from .highlight import HighlightEditor, HighlightViewer, Highlighter

from guicore.appwindow.appwindow import AppWindow

GUI_PATH = 'guicore'

FUNC_NAME = 'test.pfg'

CONF_NAME = 'test_highlighteditor.conf'

CONF_PATH = os.path.join(os.getcwd(), GUI_PATH, sys.argv[2], CONF_NAME)

FUNC_PATH = os.path.join(os.getcwd(), GUI_PATH, sys.argv[2], FUNC_NAME)

def _show_(app):

    # app.opened_functions = list()

    app.window = AppWindow()
    # app.tab = QTabWidget()

    app.editor = HighlightEditor('test')
    # HighlightEditor.conf_path = CONF_PATH

    app.window.add_central_widget(app.editor)


    # HighlightViewer.extensions = ('*.pfg',)

    # if app.editor:
    #     app.editor.show()


    def close_current_tab(index):

        app.window._tabs.setCurrentIndex(index)

        widget = app.window._tabs.currentWidget()

        app.window.remove_central_widget(widget)

        # app.opened_functions.remove(widget.path)


    def canceled():

        print('canceled')


    def accepted():

        print('accepted')


    def rejected():

        print('rejected')


    app.window._tabs.tabCloseRequested.connect(close_current_tab)

    app.editor.close_accepted.connect(accepted)

    app.editor.close_rejected.connect(rejected)

    app.editor.close_canceled.connect(canceled)

    # app.view = HighlightViewer()
    # if app.view:
    #     app.view.show()

    app.window.show()

    # sys.exit()

    # app.editor1 = HighlightEditor()
    # app.editor.show()
