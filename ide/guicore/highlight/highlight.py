import os

import sys

import json

from PyQt5.QtGui import QTextCharFormat, QSyntaxHighlighter, QColor, QCursor, QKeySequence

from PyQt5.QtCore import QRegExp, Qt, pyqtSignal

from PyQt5.QtWidgets import QFileDialog, QMessageBox

from collections import OrderedDict

from guicore.baseeditor.baseeditor import BaseEditor

from guicore.observer.observer import Observer

from configuration.configuration import ConfigMetaclass, ConfigAttribute


class HighlighterConfig(metaclass=ConfigMetaclass):

    colot_key = ConfigAttribute('color_key', str, 'color')

    weight_key = ConfigAttribute('weight_key', str, 'weight')

    pattern_key = ConfigAttribute('pattern_key', str, 'pattern')


    def __init__(self, path=None):

        if path:

            with open(path, 'r') as file:

                config_json = json.load(file)

                for name, value in config_json.items():

                    setattr(self, name, value)


class Highlighter(QSyntaxHighlighter):

    def __init__(self, document, **key_words):

        super(Highlighter, self).__init__(document)

        self._config = HighlighterConfig()

        self._key_words = key_words


    def highlightBlock(self, text):

        for key, settings in self._key_words.items():

            color = settings[self._config.color_key] if self._config.color_key in settings else ''

            weight = int(settings[self._config.weight_key]) if self._config.weight_key in settings else int()

            pattern = settings[self._config.pattern_key]

            expression = QRegExp(pattern)

            index = expression.indexIn(text, 0)

            key_format = QTextCharFormat()

            key_format.setFontWeight(weight)

            key_format.setForeground(QColor(color))

            while index >= 0:

                index = expression.pos(0)

                length = len(expression.cap(0))

                self.setFormat(index, length, key_format)

                index = expression.indexIn(text, index + length)


class HighlightEditor(BaseEditor, Observer):

    close_accepted = pyqtSignal(str)

    close_rejected = pyqtSignal()

    close_canceled = pyqtSignal()

    # save = pyqtSignal()


    def __init__(self, name):

        self._name = name 

        self._closed = False

        self._init_text = str()

        super(HighlightEditor, self).__init__()


    @property
    def name(self):

        return self._name


    @property
    def closed(self):

        return self._closed


    def setText(self, text):

        self._init_text = text

        super(HighlightEditor, self).setText(text)


    def close(self):

        close_text = self.toPlainText()

        if not self._init_text == close_text:

            answer = QMessageBox.question(
                self,
                'Save changes',
                'Would you like save changes before closing?',
                QMessageBox.Cancel | QMessageBox.No | QMessageBox.Yes, QMessageBox.Yes
            )

            if answer == QMessageBox.Cancel:

                self.close_canceled.emit()

                return False

            if answer == QMessageBox.Yes:

                self.close_accepted.emit(close_text)

            elif answer == QMessageBox.No:

                self.close_rejected.emit()

        super(HighlightEditor, self).close()

        self._closed = True

        return True


    def save(self):

        self._init_text = self.toPlainText()


class HighlightViewer(HighlightEditor):

    def __init__(self):

        super(HighlightViewer, self).__init__(None)

        self.setReadOnly(True)
        

    def close(self):

        super(BaseEditor, self).close()

        return True
