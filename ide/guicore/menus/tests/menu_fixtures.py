import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget


from guicore.menus.utility import MENU_PREFIX

from guicore.menus.menus import Menu


@pytest.fixture
def menu_name():

    return 'menu_name'


@pytest.fixture
def wrong_menu_name():

    return 'wrong_menu_name'


@pytest.fixture
def menu_arg_name(menu_name):

    return '_'.join((MENU_PREFIX, menu_name))


@pytest.fixture
def menu_title():

    return 'Menu title'


@pytest.fixture
def alter_menu_title():

    return 'Alter menu'


@pytest.fixture
def menu_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'icon/path'


@pytest.fixture
def alter_icon_path():

    return 'alter/path'


@pytest.fixture
def menu_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def alter_menu_icon(alter_icon_path):

    return QIcon(alter_icon_path)


@pytest.fixture
def menu(menu_name, menu_title, menu_parent, menu_icon):

    return Menu(menu_name, menu_title, menu_parent, menu_icon)