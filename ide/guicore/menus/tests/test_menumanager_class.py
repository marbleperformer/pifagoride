import sys

import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget, QMenu

from guicore.menus.managers import MenuManager

from guicore.menus.menus import Menu

from guicore.menus import utility


@pytest.fixture
def target():

    return QMenu()


@pytest.fixture
def menu_name():

    return 'menu_name'


@pytest.fixture
def menu_arg_name(menu_name):

    return '_'.join((utility.MENU_PREFIX, menu_name))


@pytest.fixture
def menu_title():

    return 'Menu title'


@pytest.fixture
def menu_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'some/path'


@pytest.fixture
def menu_icon(icon_path):

    return QIcon(icon_path)



@pytest.fixture
def menu(menu_name, menu_title, menu_parent, menu_icon):

    return Menu(menu_name, menu_title, menu_parent, menu_icon)


@pytest.fixture
def wrong_name():

    return 'Not menu'


@pytest.fixture
def argbased_manager(target):

    class Manager(MenuManager):

        __target__ = target

    return Manager


@pytest.fixture
def funcbased_manager(target):

    class Manager(MenuManager):

        def __gettarget__(self):

            return target

    return Manager


def setup_module(module):

    module.app = QApplication(sys.argv)


def teardown_module(module):

    module.app.exit()


def test_target_get_target(argbased_manager, target):

    manager = argbased_manager()

    trg = manager.get_target()

    assert trg == target


def test_get_target_get_target(funcbased_manager, target):

    manager = funcbased_manager()

    trg = manager.get_target()

    assert trg == target


def test_not_implemented():

    manager = MenuManager()

    with pytest.raises(NotImplementedError):

        trg = manager.get_target()


def test_add_menu(argbased_manager, menu, menu_arg_name):

    manager = argbased_manager()

    trg = manager.__target__

    manager.add_menu(menu)

    assert menu_arg_name in trg.__dict__


def test_wrong_add_menu(argbased_manager, wrong_name):

    manager = argbased_manager()

    with pytest.raises(TypeError):

        manager.add_menu(wrong_name)


def test_create_menu(argbased_manager, menu_name, menu_title, menu_icon, menu_arg_name):

    manager = argbased_manager()

    trg = trg = manager.__target__

    manager.create_menu(menu_name, menu_title, menu_icon)

    assert menu_arg_name in trg.__dict__


def test_all_menu(argbased_manager, menu):

    manager = argbased_manager()

    manager.add_menu(menu)

    menus = manager.all_menus()

    assert menu in menus


def test_get_menu(argbased_manager, menu, menu_name):

    manager = argbased_manager()

    manager.add_menu(menu)

    mn = manager.get_menu(menu_name)

    assert mn is menu


def test_remove_menu_by_menu(argbased_manager, menu, menu_arg_name):

    manager= argbased_manager()

    trg = trg = manager.__target__

    manager.add_menu(menu)

    manager.remove_menu(menu)

    assert not menu_arg_name in trg.__dict__
