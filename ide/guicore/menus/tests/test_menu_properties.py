import sys

import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget

from guicore.menus.menus import Menu

from guicore.menus import utility


@pytest.fixture
def menu_name():

    return 'menu_name'


@pytest.fixture
def menu_title():

    return 'Menu title'


@pytest.fixture
def alter_menu_title():

    return 'Alter menu'


@pytest.fixture
def menu_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'icon/path'


@pytest.fixture
def alter_icon_path():

    return 'alter/path'


@pytest.fixture
def menu_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def alter_menu_icon(alter_icon_path):

    return QIcon(alter_icon_path)


@pytest.fixture
def menu(menu_name, menu_title, menu_parent, menu_icon):

    return Menu(menu_name, menu_title, menu_parent, menu_icon)


def setup_module(module):

    module.app = QApplication(sys.argv)


def teardown_module(module):

    module.app.exit()


def test_name(menu, menu_name):

    assert menu.name == menu_name


def test_parent(menu, menu_parent):

    assert menu.parent is menu_parent


def test_title(menu, menu_title):

    assert menu.title == menu_title


def test_set_title(menu, alter_menu_title):

    menu.title = alter_menu_title

    assert menu.title == alter_menu_title


def test_icon(menu, menu_icon):

    assert menu.icon == menu_icon


def test_set_icon_with_path(menu, alter_icon_path):

    menu.icon = alter_icon_path

    assert isinstance(menu.icon, QIcon)


def test_icon_set_with_icon(menu, alter_menu_icon):

    menu.icon = alter_menu_icon

    assert menu.icon == alter_menu_icon

