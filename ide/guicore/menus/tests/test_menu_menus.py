import sys

import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget

from guicore.menus.menus import Menu

from guicore.menus import utility


@pytest.fixture
def menu_name():

    return 'menu_name'


@pytest.fixture
def wrong_menu_name():

    return 'wrong_menu_name'


@pytest.fixture
def menu_arg_name(menu_name):

    return '_'.join((utility.MENU_PREFIX, menu_name))


@pytest.fixture
def menu_title():

    return 'Menu title'


@pytest.fixture
def menu_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'icon/path'


@pytest.fixture
def menu_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def menu(menu_name, menu_title, menu_parent, menu_icon):

    return Menu(menu_name, menu_title, menu_parent, menu_icon)


@pytest.fixture
def submenu(menu_name, menu_title, menu, menu_icon):

    return Menu(menu_name, menu_title, menu, menu_icon)


def setup_module(module):

    module.app = QApplication(sys.argv)


def teardown_module(module):

    module.app.exit()


def test_add_menu(menu, submenu, menu_arg_name):

    menu.add_menu(submenu)

    assert menu_arg_name in menu.__dict__


def test_wrong_add_menu(menu, wrong_menu_name):

    with pytest.raises(TypeError):

        menu.add_menu(wrong_menu_name)


def test_create_menu(menu, menu_name, menu_title, menu_icon, menu_arg_name):

    menu.create_menu(menu_name, menu_title, menu_icon)

    assert menu_arg_name in menu.__dict__


def test_all_menu(menu, submenu):

    menu.add_menu(submenu)

    menus = menu.all_menus()

    assert submenu in menus


def test_get_menu(menu, submenu, menu_name):

    menu.add_menu(submenu)

    mn = menu.get_menu(menu_name)

    assert mn is submenu


def test_remove_menu_by_menu(menu, submenu, menu_arg_name):

    menu.add_menu(submenu)

    menu.remove_menu(submenu)

    assert not menu_arg_name in menu.__dict__