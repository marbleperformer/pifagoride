import sys

import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget


from guicore.actions.utility import ACTION_PREFIX

from guicore.menus.utility import MENU_PREFIX

from guicore.actions.actions import Action

from guicore.menus.menus import Menu


@pytest.fixture
def menu_name():

    return 'menu_name'


@pytest.fixture
def menu_title():

    return 'Menu title'


@pytest.fixture
def menu_parent():

    return QWidget()


@pytest.fixture
def menu_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def menu(menu_name, menu_title, menu_parent, menu_icon):

    return Menu(menu_name, menu_title, menu_parent, menu_icon)


# action fixtures


@pytest.fixture
def action_name():

    return 'test_action'


@pytest.fixture
def action_arg_name(action_name):

    return '_'.join((ACTION_PREFIX, action_name))


@pytest.fixture
def action_title():

    return 'Test action'


@pytest.fixture
def action_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'some/path'


@pytest.fixture
def action_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def action_func():

    return lambda: print('action triggered')


@pytest.fixture
def action(action_name, action_title, action_parent, action_icon):

    return Action(action_name, action_title, action_parent, action_icon)


@pytest.fixture
def wrong_action():

    return 'Not action'


def setup_module(module):

    module.app = QApplication(sys.argv)


def teardown_module(module):

    module.app.exit()


def test_add_action(menu, action, action_arg_name):

    menu.add_action(action)

    assert action_arg_name in menu.__dict__


def test_wrong_add_action(menu, wrong_action):

    with pytest.raises(TypeError):

        menu.add_action(wrong_action)


def test_create_action(menu, action_name, action_func, action_title, action_icon, action_arg_name):

    menu.create_action(action_name, action_func, action_title, action_icon)

    assert action_arg_name in menu.__dict__


def test_all_actions(menu, action):

    menu.add_action(action)

    actions = menu.all_actions()

    assert action in actions


def test_get_action(menu, action, action_name):

    menu.add_action(action)

    act = menu.get_action(action_name)

    assert act is action


def test_remove_action_by_action(menu, action, action_arg_name):

    menu.add_action(action)

    menu.remove_action(action)

    assert not action_arg_name in menu.__dict__