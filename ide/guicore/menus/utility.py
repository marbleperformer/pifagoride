MENU_PREFIX = '_menu'
  

def add_menu(target, menuclass, menu, prefix=MENU_PREFIX):

    if not isinstance(menu, menuclass):

        raise TypeError('Wrong menu type, it should be %s.' % menuclass)

    menu_name = '_'.join((prefix, menu.name))

    setattr(target, menu_name, menu)

    target.addMenu(menu)


def create_menu(target, menuclass, name, title=None, icon_path=None, prefix=MENU_PREFIX):

    menu =  menuclass(name, title, target, icon_path)

    menu_name = '_'.join((prefix, menu.name))

    setattr(target, menu_name, menu)

    target.addMenu(menu)

    return menu


def all_menus(target, menuclass):

    return filter(lambda itm: isinstance(itm, menuclass), target.__dict__.values())


def get_menu(target, name, prefix=MENU_PREFIX):

    menu_name = '_'.join((prefix,  name))

    return getattr(target, menu_name)


def remove_menu(target, menuclass, value, prefix=MENU_PREFIX):

    name = value.name if isinstance(value, menuclass) else value

    menu_name = '_'.join((prefix, name))

    instance = getattr(target, menu_name)

    target.removeAction(instance.menuAction())

    delattr(target, menu_name)

