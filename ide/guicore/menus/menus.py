
from PyQt5.QtWidgets import QMenu

from PyQt5.QtGui import QIcon


from guicore.actions import utility as au

from . import utility as mu


class Menu(QMenu):

    def __init__(self, name, title=None, parent=None, icon=None):

        self._name = name

        self._title = title if title else name.title().replace('_', ' ')

        self._icon = icon if isinstance(icon, QIcon) else QIcon(icon)

        super(Menu, self).__init__(self._title, parent)

        if icon:

            self.setIcon(self._icon)


    @property
    def name(self):

        return self._name


    @property
    def parent(self):

        return self.parentWidget()


    @property
    def title(self):

        return self._title


    @title.setter
    def title(self, value):

        self._title = value

        self.setTitle(self._title)


    @property
    def icon(self):

        return self._icon


    @icon.setter
    def icon(self, value):

        self._icon = value if isinstance(value, QIcon) else QIcon(value)

        if self._icon:

            self.setIcon(self._icon)


    def add_menu(self, menu):

        menuclass = self.__class__

        mu.add_menu(self, menuclass, menu)


    def create_menu(self, name, title=None, icon_path=None):

        menuclass = self.__class__

        return mu.create_menu(self, menuclass, name, title, icon_path)


    def all_menus(self):

        menuclass = self.__class__

        return mu.all_menus(self, menuclass)


    def get_menu(self, name):

        return mu.get_menu(self, name)


    def remove_menu(self, value):

        menuclass = self.__class__

        mu.remove_menu(self, menuclass, value)


    def add_action(self, action):

        au.add_action(self, action)


    def create_action(self, name, func, title=None, icon_path=None):

        return au.create_action(self, name, func, title, icon_path)


    def all_actions(self):

        return au.all_actions(self)


    def get_action(self, name):

        return au.get_action(self, name)


    def remove_action(self, value):

        au.remove_action(self, value)

