
from . import utility

from . import menus


class MenuManager():

    @classmethod
    def get_target(cls):

        if '__target__' in cls.__dict__:

            return getattr(cls, '__target__')

        if '__gettarget__' in dir(cls):

            func = getattr(cls, '__gettarget__')

            return func(cls)

        raise NotImplementedError('Please implement "__target__" static attribute or "__gettarget__" method.')


    @classmethod
    def add_menu(cls, menu):

        target = cls.get_target()

        utility.add_menu(target, menus.Menu, menu)


    @classmethod
    def create_menu(cls, name, title=None, icon_path=None):

        target = cls.get_target()

        return utility.create_menu(target, menus.Menu, name, title, icon_path)


    @classmethod
    def all_menus(cls):

        target = cls.get_target()

        return utility.all_menus(target, menus.Menu)


    @classmethod
    def get_menu(cls, name):

        target = cls.get_target()

        return utility.get_menu(target, name)


    @classmethod
    def remove_menu(cls, value):

        target = cls.get_target()

        utility.remove_menu(target, menus.Menu, value)

