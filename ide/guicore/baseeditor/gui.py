import sys

from PyQt5.QtWidgets import QApplication, QMenu, QAction 

from PyQt5.QtTest import QTest

from PyQt5.QtCore import Qt

from .baseeditor import BaseEditor 

MENU_NAME = 'menu'
ACTION_NAME = 'action'

def _show_(app):
    app.editor = BaseEditor()   

    menu = QMenu(MENU_NAME)

    action = QAction(ACTION_NAME, app.editor)

    BaseEditor.add_menu(menu)

    BaseEditor.add_action(action)

    app.editor.show()

    QTest.mouseClick(app.editor, Qt.RightButton)

