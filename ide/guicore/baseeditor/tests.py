import sys

import unittest

from importlib import reload

from PyQt5.QtWidgets import QApplication, QAction, QMenu

from PyQt5.QtTest import QTest

from PyQt5.QtCore import Qt

from .baseeditor import BaseEditor

MENU_NAME = 'menu'
ACTION_NAME = 'action'

class BaseEditorTestCase(unittest.TestCase):
    def setUp(self):
        self.app = QApplication(sys.argv)
        self.editor = BaseEditor()

    def tearDown(self):
        self.app.deleteLater()
        BaseEditor._menus = list()
        BaseEditor._actions = list()
        # reload(bs)

    def test_add_menu(self):
        with self.assertRaises(TypeError):
            action = QAction(ACTION_NAME, self.editor)
            BaseEditor.add_menu(action)

        menu = QMenu(MENU_NAME)
        BaseEditor.add_menu(menu)

        self.assertEqual(len(BaseEditor._menus), 1)

        with self.assertRaises(ValueError):
            BaseEditor.add_menu(menu)

    def test_add_action(self):
        with self.assertRaises(TypeError):
            menu = QMenu(MENU_NAME)
            BaseEditor.add_action(menu)

        action = QAction(ACTION_NAME, self.editor)

        BaseEditor.add_action(action)

        self.assertEqual(len(BaseEditor._actions), 1)

        with self.assertRaises(ValueError):
            BaseEditor.add_action(action)

    def test_remove_menu(self):
        with self.assertRaises(TypeError):
            action = QAction(ACTION_NAME, self.editor)
            BaseEditor.remove_menu(action)

        with self.assertRaises(ValueError):
            menu = QMenu(MENU_NAME)
            BaseEditor.remove_menu(menu)

        menu = QMenu(MENU_NAME)
        BaseEditor.add_menu(menu)
        self.assertEqual(len(BaseEditor._menus), 1)

        BaseEditor.remove_menu(menu)
        self.assertEqual(len(BaseEditor._menus), 0)

    def test_remove_action(self):
        with self.assertRaises(TypeError):
            menu = QMenu(MENU_NAME)
            BaseEditor.remove_action(menu)

        with self.assertRaises(ValueError):
            action = QAction(ACTION_NAME, self.editor)
            BaseEditor.remove_action(action)

        action = QAction(ACTION_NAME, self.editor)
        BaseEditor.add_action(action)
        self.assertEqual(len(BaseEditor._actions), 1)

        BaseEditor.remove_action(action)
        self.assertEqual(len(BaseEditor._actions), 0)

if __name__ == '__main__':
    unittest.main()