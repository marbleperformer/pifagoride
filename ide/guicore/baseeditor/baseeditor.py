from PyQt5.QtWidgets import QTextEdit, QMenu, QAction

from PyQt5.QtCore import Qt

from PyQt5.QtGui import QCursor

from guicore.actions.actions import Action

from guicore.actionmanager.actionmanager import StaticActionManagerMixin

from guicore.menumanager.menumanager import StaticMenuManagerMixin

# WRONG_TYPE_ERROR_TEXT = 'Wrong type argument "%s". It should be a "%s".'

# MENU_EXISTS_ERROR_TEXT = 'Menu "%s" already exists into "%s" context.'
# ACTION_EXISTS_ERROR_TEXT = 'Action "%s" already exists into "%s" context.'

# MENU_NOT_EXISTS_ERROR_TEXT = 'Menu "%s" not exists into "%s" context.'
# ACTION_NOT_EXISTS_ERROR_TEXT = 'Action "%s" not exists into "%s" context.'

# UNSUPPORTED_TARGET_ERROR_TEXT = 'Unsupproted target "%s". Target should be "repository", "function" or "rang".'

# UNSUPPORTED_METHOD_ERROR_TEXT = 'Unsupported method "%s" for target "%s".'

class BaseEditor(QTextEdit, StaticActionManagerMixin):
    # _menus = list()
    # _actions = list()

    def __init__(self):

        super(BaseEditor, self).__init__()

        self.setContextMenuPolicy(Qt.CustomContextMenu)

        self.customContextMenuRequested.connect(self._open_menu)


    @staticmethod
    def __staticsuperaction__():

        return Action


    @classmethod
    def __staticactionbar__(cls):

        if not '_menu' in cls.__dict__:

            cls._menu = QMenu()

        return cls._menu


    @classmethod
    def __statictarget__(cls):

        return cls

    # @classmethod
    # def add_menu(cls, menu):
    #     if not isinstance(menu, QMenu):
    #         raise TypeError(WRONG_TYPE_ERROR_TEXT % ('menu', QMenu))

    #     if menu in cls._menus:
    #         raise ValueError(MENU_EXISTS_ERROR_TEXT % (menu, cls))

    #     cls._menus.append(menu)

    # @classmethod
    # def add_action(cls, action):
    #     if not isinstance(action, QAction):
    #         raise TypeError(WRONG_TYPE_ERROR_TEXT % ('action', QMenu))

    #     if action in cls._actions:
    #         raise ValueError(MENU_EXISTS_ERROR_TEXT % (action, cls))

    #     cls._actions.append(action)

    # @classmethod
    # def remove_menu(cls, menu):
    #     if not isinstance(menu, QMenu):
    #         raise TypeError(WRONG_TYPE_ERROR_TEXT % ('menu', QMenu))

    #     if not menu in cls._menus:
    #         raise ValueError(MENU_NOT_EXISTS_ERROR_TEXT % (menu, cls))

    #     cls._menus.remove(menu)

    # @classmethod
    # def remove_action(cls, action):
    #     if not isinstance(action, QAction):
    #         raise TypeError(WRONG_TYPE_ERROR_TEXT % ('action', QMenu))

    #     if not action in cls._actions:
    #         raise ValueError(ACTION_NOT_EXISTS_ERROR_TEXT % (action, cls))

    #     cls._actions.remove(action)

    def _open_menu(self):

        menu = self.__staticactionbar__()

        # self_cls = type(self)

        # menu = self_cls._menu

        menu.exec_(QCursor.pos())

        # context_menu = QMenu()

        # if len(BaseEditor._menus) > 0 or len(BaseEditor._actions) > 0:

        #     for action in BaseEditor._actions:

        #         context_menu.addAction(action)

        #     for menu in BaseEditor._menus:

        #         context_menu.addMenu(menu)

        #     context_menu.exec_(QCursor.pos())
