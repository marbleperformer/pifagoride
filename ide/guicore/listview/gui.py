from .listview import ListView

from guicore.functionitems.functionitems import ListFunctionItem

from guicore.actions.actions import Action


FUNCTION_TITLE = 'Function1'

FUNCTION_PATH = 'path/to/func'

ITEM_ACTION_TITLE = 'context action'

VIEW_ACTION_TITLE = 'view action'


def _show_(app):

    app.view = ListView()


    item_action = Action(ITEM_ACTION_TITLE, app.view)

    view_action = Action(VIEW_ACTION_TITLE, app.view)


    ListView.add_static_action(view_action)

    ListFunctionItem.add_static_action(item_action)


    app.view.create_item(FUNCTION_TITLE, FUNCTION_PATH)
    
    app.view.create_item(FUNCTION_TITLE, FUNCTION_PATH)
    
    app.view.create_item(FUNCTION_TITLE, FUNCTION_PATH)

    app.view.show()
