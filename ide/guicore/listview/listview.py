import os

from PyQt5.QtGui import QStandardItemModel, QCursor

from PyQt5.QtCore import QSortFilterProxyModel, Qt

from PyQt5.QtWidgets import QListView, QMenu

from guicore.actionmanager.actionmanager import StaticActionManagerMixin

from guicore.itemmanager.itemmanager import ItemManagerMixin

from guicore.functionitems.functionitems import ListFunctionItem

from guicore.actions.actions import Action


class BaseItemView(object):

    def __init__(self, *args, **kwargs):

        super(BaseItemView, self).__init__(*args, **kwargs)

        self._set_view_models()

        self._item_selected = False

        self._selected_item = None

        self.setContextMenuPolicy(Qt.CustomContextMenu)

        self.customContextMenuRequested.connect(self._open_menu)


    @property
    def selected_item(self):

        return self._selected_item


    def _set_view_models(self):

        self._source_model = QStandardItemModel()

        self._proxy_model = QSortFilterProxyModel()

        self._proxy_model.setSourceModel(self._source_model)

        self.setModel(self._proxy_model)


    def selectionChanged(self, selected, deselected):

        if selected:

            indexes = selected.indexes()

            proxy_idx = indexes[0]

            source_idx = self._proxy_model.mapToSource(proxy_idx)

            self._selected_item = self._source_model.itemFromIndex(source_idx)

        super(BaseItemView, self).selectionChanged(selected, deselected)


    def _open_menu(self, idx):

        self_cls = type(self)

        itm_cls = type(self._selected_item)

        menu = itm_cls._menu if self._selected_item else self_cls._menu

        self._selected_item = False

        menu.exec_(QCursor.pos())


class ListView(StaticActionManagerMixin, ItemManagerMixin, BaseItemView, QListView):

    # def __new__(cls, *args, **kwargs):

    #     cls._menu = QMenu()

    #     return super().__new__(cls)


    def __init__(self, parent=None):

        super(ListView, self).__init__(parent)


    @staticmethod
    def __staticsuperaction__():

        return Action


    @classmethod
    def __staticactionbar__(cls):

        if not '_menu' in cls.__dict__:

            cls._menu = QMenu()

        return cls._menu


    @classmethod
    def __statictarget__(cls):

        return cls


    def __itembar__(self):

        return self._source_model


    def __target__(self):

        return self


    def __superitem__(self):

        return ListFunctionItem


    def _get_function_name(self, repository, function):

        fnc_path = function.replace(repository, '')

        fnc_short = fnc_path.replace('.pfg', '')

        fnc_shorter = fnc_short.strip('/')

        fnc_name = fnc_shorter.replace('/', '.')

        return fnc_name


    def init_repository(self, path, functions):

        item_class = self.__superitem__()

        for fnc_name in functions:

            print(fnc_name)

            # fnc_filename = os.path.basename(fnc_path)

            # fnc_name, fnc_extension = os.path.splitext(fnc_filename)

            self.create_item(fnc_name)
