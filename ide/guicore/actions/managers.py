from . import actions

from . import utility


class ActionManager():

    @classmethod
    def get_target(cls):

        if '__target__' in cls.__dict__:

            return getattr(cls, '__target__')

        if '__gettarget__' in dir(cls):

            func = getattr(cls, '__gettarget__')

            return func(cls)

        raise NotImplementedError('Please implement "__target__" static attribute or "__get_target__" method.')


    @classmethod
    def add_action(cls, action):

        target = cls.get_target()

        utility.add_action(target, action)


    @classmethod
    def create_action(cls, name, func, title=None, icon_path=None):

        target = cls.get_target()

        return utility.create_action(target, name, func, title, icon_path)


    @classmethod
    def all_actions(cls):

        target = cls.get_target()

        return utility.all_actions(target)


    @classmethod
    def get_action(cls, name):

        target = cls.get_target()

        return utility.get_action(target, name)


    @classmethod
    def remove_action(cls, value):

        target = cls.get_target()

        utility.remove_action(target, value)

