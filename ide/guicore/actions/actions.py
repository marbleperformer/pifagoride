from PyQt5.QtWidgets import QAction

from PyQt5.QtGui import QIcon


class Action(QAction):

    def __init__(self, name, title=None, parent=None, icon=None):

        self._name = name

        self._title = title if title else name.replace('_', ' ')

        self._icon = QIcon(icon) if isinstance(icon, str) else icon

        super(Action, self).__init__(self._title, parent)

        if self._icon:

            self.setIcon(self._icon)


    @property
    def name(self):

        return self._name


    @property
    def parent(self):

        return self.parentWidget()


    @property
    def title(self):

        return self._title


    @title.setter
    def title(self, value):

        self._title = value

        self.setText(self._title)


    @property
    def icon(self):

        return self._icon


    @icon.setter
    def icon(self, value):

        self._icon = QIcon(value) if isinstance(value, str) else value

        if self._icon:

            self.setIcon(self._icon)


    def bind(self, function):

        self.triggered.connect(function)
