import sys

import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget, QMenu

from guicore.actions.managers import ActionManager

from guicore.actions.actions import Action

from guicore.actions import utility


@pytest.fixture
def target():

    return QMenu()


@pytest.fixture
def action_name():

    return 'test_action'


@pytest.fixture
def action_arg_name(action_name):

    return '_'.join((utility.ACTION_PREFIX, action_name))


@pytest.fixture
def action_title():

    return 'Test action'


@pytest.fixture
def action_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'some/path'


@pytest.fixture
def action_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def action_func():

    return lambda: print('action triggered')


@pytest.fixture
def action(action_name, action_title, action_parent, action_icon):

    return Action(action_name, action_title, action_parent, action_icon)


@pytest.fixture
def wrong_action():

    return 'Not action'


@pytest.fixture
def argbased_manager(target):

    class Manager(ActionManager):

        __target__ = target

    return Manager


@pytest.fixture
def funcbased_manager(target):

    class Manager(ActionManager):

        def __gettarget__(self):

            return target

    return Manager


def setup_module(module):

    module.app = QApplication(sys.argv)


def teardown_module(module):

    module.app.exit()


def test_target_get_target(argbased_manager, target):

    trg = argbased_manager.get_target()

    assert trg == target


def test_get_target_get_target(funcbased_manager, target):

    trg = funcbased_manager.get_target()

    assert trg == target


def test_not_implemented():

    with pytest.raises(NotImplementedError):

        trg = ActionManager.get_target()


def test_add_action(argbased_manager, action, action_arg_name):

    trg = argbased_manager.__target__

    argbased_manager.add_action(action)

    assert action_arg_name in trg.__dict__


def test_wrong_add_action(argbased_manager, wrong_action):

    with pytest.raises(TypeError):

        argbased_manager.add_action(wrong_action)


def test_create_action(argbased_manager, action_name, action_func, action_title, action_icon, action_arg_name):

    trg = trg = argbased_manager.__target__

    argbased_manager.create_action(action_name, action_func, action_title, action_icon)

    assert action_arg_name in trg.__dict__


def test_all_actions(argbased_manager, action):

    argbased_manager.add_action(action)

    actions = argbased_manager.all_actions()

    assert action in actions


def test_get_action(argbased_manager, action, action_name):

    argbased_manager.add_action(action)

    act = argbased_manager.get_action(action_name)

    assert act is action


def test_remove_action_by_action(argbased_manager, action, action_arg_name):

    trg = trg = argbased_manager.__target__

    argbased_manager.add_action(action)

    argbased_manager.remove_action(action)

    assert not action_arg_name in trg.__dict__

