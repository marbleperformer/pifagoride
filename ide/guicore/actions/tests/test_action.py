import sys

import pytest

from PyQt5.QtGui import QIcon

from PyQt5.QtWidgets import QApplication, QWidget

from guicore.actions.actions import Action


@pytest.fixture
def action_name():

    return 'test_action'


@pytest.fixture
def action_title():

    return 'Test action'


@pytest.fixture
def alter_action_title():

    return 'Some action'


@pytest.fixture
def action_parent():

    return QWidget()


@pytest.fixture
def icon_path():

    return 'test/path'


@pytest.fixture
def alter_icon_path():

    return 'some/path'


@pytest.fixture
def action_icon(icon_path):

    return QIcon(icon_path)


@pytest.fixture
def alter_action_icon(alter_icon_path):

    return QIcon(alter_icon_path)


@pytest.fixture
def action_func_text():

    return 'action triggered'


@pytest.fixture
def action_func(action_func_text):

    return lambda: print(action_func_text)


@pytest.fixture
def action(action_name, action_title, action_parent, action_icon):

    return Action(action_name, action_title, action_parent, action_icon)


def setup_module(module):

    module.app = QApplication(sys.argv)


def teardown_module(module):

    module.app.exit()


def test_name(action, action_name):

    assert action.name == action_name


def test_parent(action, action_parent):

    assert action.parent is action_parent


def test_title(action, action_title):

    assert action.title == action_title


def test_title_set(action, alter_action_title):

    action.title = alter_action_title

    assert action.title == alter_action_title


def test_icon(action, action_icon):

    assert action.icon == action_icon


def test_icon_set_with_path(action, alter_icon_path):

    action.icon = alter_icon_path

    assert isinstance(action.icon, QIcon)


def test_icon_set_with_icon(action, alter_action_icon):

    action.icon = alter_action_icon

    assert action.icon == alter_action_icon


def test_bind(action, action_func, action_func_text, capsys):

    action.bind(action_func)

    action.trigger()

    out, err = capsys.readouterr()

    assert action_func_text in out
