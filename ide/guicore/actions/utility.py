from . import actions


ACTION_PREFIX = '_act'


def add_action(target, action, prefix=ACTION_PREFIX):

    if not isinstance(action, actions.Action):

        raise TypeError('Wrong action type, it should be %s.' % actions.Action)

    action_name = '_'.join((prefix, action.name))

    setattr(target, action_name, action)

    target.addAction(action)


def create_action(target, name, func, title=None, icon=None, prefix=ACTION_PREFIX):

    action = actions.Action(name, title, target, icon)

    action.bind(func)

    action_name = '_'.join((prefix, action.name))

    setattr(target, action_name, action)

    target.addAction(action)

    return action


def all_actions(target):

    return filter(lambda itm: isinstance(itm, actions.Action), target.__dict__.values())


def get_action(target, name, prefix=ACTION_PREFIX):

    action_name = '_'.join((prefix,  name))

    return getattr(target, action_name)


def remove_action(target, value, prefix=ACTION_PREFIX):

    name = value.name if isinstance(value, actions.Action) else value

    action_name = '_'.join((prefix, name))

    instance = getattr(target, action_name)

    target.removeAction(instance)

    delattr(target, action_name)

