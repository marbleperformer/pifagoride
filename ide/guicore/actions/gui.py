from PyQt5.QtWidgets import QMainWindow

from .actions import Action, Menu

def _show_(app):
    app.window = QMainWindow()

    app.menubar = app.window.menuBar()

    app.menu = Menu('test')

    app.action = Action('Action1', app.window)
    app.action2 = Action('Action2', app.window)

    app.menu.add_action(app.action)
    app.menu.add_action(app.action2)

    # app.menu.remove_action(app.action2)

    app.menubar.addMenu(app.menu)

    app.window.show()
