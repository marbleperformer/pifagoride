import unittest

# from PyQt5.QtWidgets import QMainWindow

from PyQt5.QtWidgets import QTreeView

from guicore.repositoryitems.repositoryitems import RepositoryItem

from PyQt5.QtGui import QStandardItemModel

from PyQt5.QtCore import QSortFilterProxyModel

# from guicore.RepositoryItems.RepositoryItems import RepositoryItem

from .itemmanager import ItemManagerMixin


ITEMBAR_NAME = 'Test'

ITEM_TITLE = 'Some item'

ITEM_NAME = 'some_item'

WRONG_ITEM_NAME = 'wrong_item_name'

ITEM_STRING = 'item'

ITEM_PATH = ''

# ICON_PATH = 'static/Translate.png'

WRONG_REMOVE_VALUE = 123


class DefaultItemRequired:

    testcase = None

    def __init__(self, func):

        self._func =  func

    def __call__(self, *args, **kwargs):

        global default_item

        default_item = self.testcase.manager.create_item(ITEM_TITLE, ITEM_PATH) 

        return self._func(self.testcase, *args, **kwargs)


class ItemManagerMixinTestCase(unittest.TestCase):

    def setUp(self):

        class Manager(ItemManagerMixin, QTreeView):

            def __init__(self):

                super(Manager, self).__init__()

                self._source_model = QStandardItemModel()

                self._proxy_model = QSortFilterProxyModel()


            @staticmethod
            def __superitem__():

                return RepositoryItem

            def __itembar__(self):

                return self._source_model

            def __target__(self):

                return self

        self.manager = Manager()

        DefaultItemRequired.testcase = self


    def test_not_implemented_superitem(self):

        class Manager(ItemManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__superitem__()


    def test_not_implemented_itembar(self):

        class Manager(ItemManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__itembar__()


    def test_not_implemented_target(self):

        class Manager(ItemManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__target__()


    def test_create_item(self):

        item = self.manager.create_item(ITEM_TITLE, ITEM_PATH)
        
        attribute = self.manager.get_item(ITEM_NAME)

        self.assertIs(item, attribute)


    @DefaultItemRequired
    def test_children(self):

        length = len(tuple(self.manager.children))

        self.assertGreater(length, 0)


    @DefaultItemRequired
    def test_get_item(self):

        item = self.manager.get_item(ITEM_NAME)

        self.assertIsNotNone(item)


    @DefaultItemRequired
    def test_get_wrong_item(self):

        with self.assertRaises(ValueError):

            self.manager.get_item(WRONG_ITEM_NAME)


    def test_add_item(self):

        item = RepositoryItem(ITEM_TITLE, ITEM_PATH, self.manager)

        self.manager.add_item(item)

        attribute = self.manager.get_item(ITEM_NAME)

        self.assertIsNotNone(attribute)


    def test_add_wrong_item(self):

        with self.assertRaises(TypeError):

            self.manager.add_item(ITEM_STRING)


    @DefaultItemRequired
    def test_remove_item(self):

        self.manager.remove_item(ITEM_NAME)

        items = self.manager.children

        self.assertEqual(len(tuple(items)), 0)


    @DefaultItemRequired
    def test_remove_item_by_item(self):

        attribute = self.manager.get_item(ITEM_NAME)

        self.manager.remove_item(value=attribute)

        items = self.manager.children

        self.assertEqual(len(tuple(items)), 0)


    @DefaultItemRequired
    def test_remove_wrong_item(self):

        with self.assertRaises(TypeError):

            self.manager.remove_item(WRONG_REMOVE_VALUE)

    @DefaultItemRequired
    def test_remove_not_exist_item(self):

        with self.assertRaises(ValueError):

            self.manager.remove_item(WRONG_ITEM_NAME)

    @DefaultItemRequired
    def test_remove_not_exist_item_by_item(self):

        item = RepositoryItem(WRONG_ITEM_NAME, ITEM_PATH, self.manager)

        with self.assertRaises(ValueError):

            self.manager.remove_item(item)
