import json, os
from PyQt5.QtGui import QStandardItem


INTERFACE_ERROR_TEXT = '"%s" should be overrided before use.'

ITEM_NOT_EXITS_ERROR_TEXT = 'Item "%s" not exist into "%s" context.'

# ITEM_EXITS_ERROR_TEXT = 'Item "%s" already exist into "%s" context.'

WRONG_TYPE_ERROR_TEXT = 'Wrong type argument "%s", it should be "%s".'

NOT_ITEM_AND_STRING_ERROR_TEXT = 'Wrong type argument "value", it should be "%s" or "%s".'

ITEM_SIGN = '_item'


class ItemManagerMixin:


	def __itembar__(self):

		raise NotImplementedError(INTERFACE_ERROR_TEXT % '__itembar__')


	def __target__(self):

		raise NotImplementedError(INTERFACE_ERROR_TEXT % '__target__')


	def __superitem__(self):

		raise NotImplementedError(INTERFACE_ERROR_TEXT % '__superitem__')


	@property
	def children(self):

		target = self.__target__()

		for field_name in target.__dict__:

			if field_name.endswith(ITEM_SIGN):

				yield target.__dict__.get(field_name)


	def get_item(self, name):

		target = self.__target__()

		item_name = name + ITEM_SIGN

		if not item_name in self.__dict__:

			raise ValueError(ITEM_NOT_EXITS_ERROR_TEXT % (item_name, target))

		return getattr(target, item_name)

		# item = getattr(self, root_name)

		# if name_iter:

		# 	new_full_name = '.'.join(name_iter)

		# 	item = item.get_item(new_full_name)

		# return item


	def create_item(self, title):


		# name_iter, path_iter = name.split('.'), path.split('/')

		# root_name, root_path = name_iter.pop(0), path_iter.pop(0)

		target = self.__target__()

		item_class = self.__superitem__()

		item = item_class(title, self)

		item_name = item.name + ITEM_SIGN

		setattr(target, item_name, item)

		self.__itembar__().appendRow(item)

		return item

		# if root_name in target.__dict__:

		# 	item = getattr(target, root_name)

		# else:

		# 	new_path = os.path.join(root_path)

		# 	item = item_class(root_name, new_path)

		# 	setattr(target, root_name, item)

		# 	target.appendRow(item)

		# if name_iter and path_iter:

		# 	new_full_name = '.'.join(name_iter)

		# 	new_path = os.path.join(root_path)

		# 	item = item.create_item(new_full_name, new_path)

		# return item


	def add_item(self, item):

		target = self.__target__()

		item_class = self.__superitem__()

		if not isinstance(item, item_class):

			raise TypeError(WRONG_TYPE_ERROR_TEXT % ('item', item_class))

		item_name = item.name + ITEM_SIGN

		setattr(target, item_name, item)

		self.__itembar__().appendRow(item)


		# item_class = self.__superitem__()

		# if not isinstance(item, item_class):

		# 	raise TypeError(WRONG_TYPE_ERROR_TEXT % ('item', item_class))

		# item_name = item.name + ITEM_SIGN

		# setattr(self.__target__(), item_name, item)

		# self


		# children = self.__children__()

		# identity = item.__identity__()

		# if identity in children:

		# 	raise ValueError(ITEM_EXITS_ERROR_TEXT % (item, self))

		# item.parent = self

		# self.appendRow(item)

		# self.children.append(identity)


	def remove_item(self, value):

		target = self.__target__()

		item_class = self.__superitem__()

		if not isinstance(value, str) and not isinstance(value, item_class):

			raise TypeError(NOT_ITEM_AND_STRING_ERROR_TEXT % (item_class, str))

		item_name = (value.name if isinstance(value, item_class) else value) + ITEM_SIGN

		if not item_name in target.__dict__:

			raise ValueError(ITEM_NOT_EXITS_ERROR_TEXT % (item_name, target))

		item_isntance = (value if isinstance(value, item_class) else self.get_item(value))

		self.__itembar__().removeRow(item_isntance.index().row())

		delattr(target, item_name)

		return item_isntance

# 	def create(self, full_name):

# 		name_iter = full_name.split('.')

# 		root_name = name_iter.pop(0)

# 		if root_name in self.__children__():

# 			item = 




# 	def remove(self, item):

# 		if not item.__identity__ in self.__children__():

# 			raise ValueError(ITEM_NOT_EXITS_ERROR_TEXT % (item, self))



# 		self.children.remove(item.identity)
# 		self.removeRow(item.index().row())

# class FunctionItem(TreeItem):
# 	def __init__(self, repository, **attrs):
# 		super(FunctionItem, self).__init__()

# 		self.repository = repository

# 		self.__dict__.update(attrs)

# 		self.setText(attrs['name'])

# 	@property
# 	def identity(self):
# 		return self.url

# 	@property
# 	def full_name(self):
# 		return '.'.join((self.parent.full_name, self.name)) if isinstance(self.parent, FunctionItem) else self.name

# class RepositoryItem(TreeItem):
# 	def __init__(self, serializer, **attrs):
# 		super(RepositoryItem, self).__init__()

# 		self.serializer = serializer

# 		self.__dict__.update(attrs)
		
# 		self.setText(attrs['name'])

# 	@property
# 	def identity(self):
# 		return self.url
# 		