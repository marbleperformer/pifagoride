import unittest

from PyQt5.QtWidgets import QMainWindow, QMenu

from guicore.actions.actions import Action

from .actionmanager import ActionManagerMixin, StaticActionManagerMixin


ACTION_TITLE = 'Some action'

ACTION_NAME = 'some_action'

WRONG_ACTION_NAME = 'wrong_action_name'

ACTION_STRING = 'action'

ICON_PATH = 'static/Translate.png'

WRONG_REMOVE_VALUE = 123

STATIC_MENU_TITLE = 'static menu'


class DefaultActionRequired:

    testcase = None

    def __init__(self, func):

        self._func =  func

    def __call__(self, *args, **kwargs):
        def action_function():
            pass

        global default_action

        default_action = self.testcase.manager.create_action(ACTION_TITLE, action_function, ICON_PATH) 

        return self._func(self.testcase, *args, **kwargs)


class ActionManagerTestCase(unittest.TestCase):

    def setUp(self):

        class Manager(ActionManagerMixin, QMainWindow):
            @staticmethod
            def __superaction__():
                return Action

            def __actionbar__(self):
                return self.menuBar()

            def __target__(self):
                return self

        self.manager = Manager()

        DefaultActionRequired.testcase = self


    def test_not_implemented_superaction(self):

        class Manager(ActionManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__superaction__()


    def test_not_implemented_actionbar(self):

        class Manager(ActionManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__actionbar__()


    def test_not_implemented_target(self):

        class Manager(ActionManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__target__()


    @DefaultActionRequired
    def test_actions(self):

        length = len(tuple(self.manager.actions))

        self.assertGreater(length, 0)


    def test_create_action(self):

        def action_function():
            pass

        action = self.manager.create_action(ACTION_TITLE, action_function)
        
        attribute = self.manager.get_action(ACTION_NAME)

        self.assertIs(action, attribute)


    def test_create_action_with_icon(self):

        def action_function():
            pass

        action = self.manager.create_action(ACTION_TITLE, action_function, ICON_PATH)

        attribute = self.manager.get_action(ACTION_NAME)

        self.assertIs(action, attribute)


    @DefaultActionRequired
    def test_get_action(self):

        action = self.manager.get_action(ACTION_NAME)

        self.assertIsNotNone(action)


    @DefaultActionRequired
    def test_get_wrong_action(self):

        with self.assertRaises(ValueError):

            self.manager.get_action(WRONG_ACTION_NAME)


    def test_add_action(self):

        action = Action(ACTION_TITLE, self.manager)

        self.manager.add_action(action)

        attribute = self.manager.get_action(ACTION_NAME)

        self.assertIsNotNone(attribute)


    def test_add_wrong_action(self):

        with self.assertRaises(TypeError):

            self.manager.add_action(ACTION_STRING)


    @DefaultActionRequired
    def test_remove_action(self):

        self.manager.remove_action(ACTION_NAME)

        actions = self.manager.__actionbar__().actions()

        self.assertEqual(len(actions), 0)


    @DefaultActionRequired
    def test_remove_action_by_action(self):

        attribute = self.manager.get_action(ACTION_NAME)

        self.manager.remove_action(value=attribute)

        actions = self.manager.__actionbar__().actions()

        self.assertEqual(len(actions), 0)


    @DefaultActionRequired
    def test_remove_wrong_action(self):

        with self.assertRaises(TypeError):

            self.manager.remove_action(WRONG_REMOVE_VALUE)


    @DefaultActionRequired
    def test_remove_not_exist_action(self):

        with self.assertRaises(ValueError):

            self.manager.remove_action(WRONG_ACTION_NAME)


    @DefaultActionRequired
    def test_remove_not_exist_action_by_action(self):

        action = Action(WRONG_ACTION_NAME, self.manager)

        with self.assertRaises(ValueError):

            self.manager.remove_action(action)


class DefaultStaticActionRequired:

    testcase = None

    def __init__(self, func):

        self._func =  func

    def __call__(self, *args, **kwargs):

        def action_function():
            pass

        global default_action

        parent = QMainWindow()

        default_action = Action(ACTION_TITLE, parent)

        self.testcase.manager.add_static_action(default_action) 

        return self._func(self.testcase, *args, **kwargs)


class StaticActionManagerTestCase(unittest.TestCase):

    def setUp(self):

        class Manager(StaticActionManagerMixin, QMainWindow):

            menu = QMenu()

            @staticmethod
            def __staticsuperaction__():
                return Action


            @classmethod
            def __staticactionbar__(cls):
                return cls.menu


            @classmethod
            def __statictarget__(cls):
                return cls

        self.manager = Manager

        DefaultStaticActionRequired.testcase = self


    def test_not_implemented_superaction(self):

        class Manager(StaticActionManagerMixin):
            pass

        with self.assertRaises(NotImplementedError):

            Manager.__staticsuperaction__()


    def test_not_implemented_actionbar(self):

        class Manager(StaticActionManagerMixin):
            pass

        with self.assertRaises(NotImplementedError):

            Manager.__staticactionbar__()


    def test_not_implemented_target(self):

        class Manager(StaticActionManagerMixin):
            pass

        with self.assertRaises(NotImplementedError):

            Manager.__statictarget__()


    @DefaultStaticActionRequired
    def test_get_actions(self):

        length = len(tuple(self.manager.get_static_actions()))

        self.assertGreater(length, 0)


    @DefaultStaticActionRequired
    def test_get_action(self):

        action = self.manager.get_static_action(ACTION_NAME)

        self.assertIsNotNone(action)


    def test_get_wron_action(self):

        with self.assertRaises(ValueError):

            self.manager.get_static_action(WRONG_ACTION_NAME)


    def test_add_static_action(self):

        parent = QMainWindow()

        action = Action(ACTION_NAME, parent)

        self.manager.add_static_action(action)

        attribute = self.manager.get_static_action(ACTION_NAME)


    def test_add_wrong_static_action(self):

        with self.assertRaises(TypeError):

            self.manager.add_static_action(ACTION_STRING)


    @DefaultStaticActionRequired
    def test_remove_static_action(self):

        self.manager.remove_static_action(ACTION_NAME)

        actions = self.manager.__staticactionbar__().actions()

        self.assertEqual(len(actions), 0)


    @DefaultStaticActionRequired
    def test_remove_static_action_by_action(self):

        action = self.manager.get_static_action(ACTION_NAME)

        self.manager.remove_static_action(value=action)

        actions = self.manager.__staticactionbar__().actions()

        self.assertEqual(len(actions), 0)


    @DefaultStaticActionRequired
    def test_remove_wrong_static_action(self):

        with self.assertRaises(TypeError):

            self.manager.remove_static_action(WRONG_REMOVE_VALUE)


    @DefaultStaticActionRequired
    def test_remove_not_exist_static_action(self):

        with self.assertRaises(ValueError):

            self.manager.remove_static_action(WRONG_ACTION_NAME)


    @DefaultStaticActionRequired
    def test_remove_not_exist_static_action_by_action(self):

        parent = QMainWindow()

        action = Action(WRONG_ACTION_NAME, parent)

        with self.assertRaises(ValueError):

            self.manager.remove_static_action(action)
