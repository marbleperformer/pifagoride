ACTION_SIGN = '_action'

STATIC_ACTION_SIGN = '_staticaction'

INTERFACE_ERROR_TEXT = '"%s" should be overrided before use.'

WRONG_TYPE_ERROR_TEXT = 'Wrong type argument "%s", it should be "%s".'

ACTION_NOT_EXITS_ERROR_TEXT = 'Action "%s" not exist into "%s" context.'

NOT_ACTION_AND_STRING_ERROR_TEXT = 'Wrong type argument "value", it should be "%s" or "%s".'


class ActionManagerMixin:

    @staticmethod
    def __superaction__():

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__superaction__')


    def __actionbar__(self):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__actionbar__')


    def __target__(self):
        
        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__target__')


    @property
    def actions(self):

        target = self.__target__()

        for field_name in target.__dict__:

            if field_name.endswith(ACTION_SIGN):

                yield target.__dict__.get(field_name)


    def get_action(self, name):

        target = self.__target__()

        action_name = name + ACTION_SIGN

        if not action_name in target.__dict__:
            raise ValueError(ACTION_NOT_EXITS_ERROR_TEXT % (name, target))

        return getattr(target, action_name)


    def create_action(self, name, title, func, icon_path=None):

        action_class = self.__superaction__()

        action = action_class(name, title, self, icon_path)

        action_name = action.name + ACTION_SIGN

        action.bind(func)

        setattr(self.__target__(), action_name, action)

        self.__actionbar__().addAction(action)

        return action

    
    def add_action(self, action):

        if not isinstance(action, self.__superaction__()):

            raise TypeError(WRONG_TYPE_ERROR_TEXT % ('action', self.__superaction__()))

        action_name = action.name + ACTION_SIGN

        setattr(self.__target__(), action_name, action)

        self.__actionbar__().addAction(action)

    
    def remove_action(self, value):

        target = self.__target__()

        action_class = self.__superaction__()

        if not isinstance(value, str) and not isinstance(value, action_class):

            raise TypeError(NOT_ACTION_AND_STRING_ERROR_TEXT % (action_class, str))

        action_name = (value.name if isinstance(value, action_class) else value) + ACTION_SIGN

        if not action_name in target.__dict__:

            raise ValueError(ACTION_NOT_EXITS_ERROR_TEXT % (action_name, target))

        action_instance = value if isinstance(value, action_class) else self.get_action(value)

        self.__actionbar__().removeAction(action_instance)

        delattr(target, action_name)

        return action_instance


class StaticActionManagerMixin():

    @staticmethod
    def __staticsuperaction__():

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__staticsuperaction__')


    @classmethod
    def __staticactionbar__(cls):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__staticactionbar__')


    @classmethod
    def __statictarget__(cls):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__statictarget__')


    @classmethod
    def get_static_actions(cls):

        target = cls.__statictarget__()

        for field_name in target.__dict__:

            if field_name.endswith(STATIC_ACTION_SIGN):

                yield target.__dict__.get(field_name)


    @classmethod
    def get_static_action(cls, name):

        target = cls.__statictarget__()

        action_name = name + STATIC_ACTION_SIGN

        if not action_name in target.__dict__:

            raise ValueError(ACTION_NOT_EXITS_ERROR_TEXT % (name, target))

        return getattr(target, action_name)


    @classmethod
    def add_static_action(cls, action):

        superaction = cls.__staticsuperaction__()

        if not isinstance(action, superaction):

            raise TypeError(WRONG_TYPE_ERROR_TEXT % ('action', superaction))

        action_name = action.name + STATIC_ACTION_SIGN

        setattr(cls.__statictarget__(), action_name, action)

        cls.__staticactionbar__().addAction(action)


    @classmethod
    def remove_static_action(cls, value):

        target = cls.__statictarget__()

        action_class = cls.__staticsuperaction__()

        if not isinstance(value, str) and not isinstance(value, action_class):

            raise TypeError(NOT_ACTION_AND_STRING_ERROR_TEXT % (action_class, str))

        action_name = (value.name if isinstance(value, action_class) else value) + STATIC_ACTION_SIGN

        if not action_name in target.__dict__:

            raise ValueError(ACTION_NOT_EXITS_ERROR_TEXT % (action_name, target))

        action_instance = (value if isinstance(value, action_class) else cls.get_static_action(value))

        cls.__staticactionbar__().removeAction(action_instance)

        delattr(target, action_name)

        return action_instance
