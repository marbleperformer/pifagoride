import sys
import unittest
from PyQt5.QtCore import Qt
from PyQt5.QtTest import QTest
from PyQt5.QtWidgets import QApplication, QWidget, QMenu, QAction

from .appwindow import AppWindow

from guicore.actions.actions import Action, Menu

LEFT_POSITION = 'left'
RIGHT_POSITION = 'right'
TOP_POSITION = 'top'
BOTTOM_POSITION = 'bottom'
WRONG_POSITION = 'wrong'

MENU_NAME = 'Menu'
WRONG_MENU_NAME = 123123123
# MENU_ACTIONS = {'some_action1':lambda   'some_action2':}

ACTION_NAME = 'Action'

STATUS_TEXT = 'Status text'
WRONG_STATUS_TEXT = 1233213

class AppWindowTestCase(unittest.TestCase):
    def setUp(self):
        self.app = QApplication(sys.argv)
        self.window = AppWindow()

    def tearDown(self):
        self.app.deleteLater()

    def test_add_perimeter_widget(self):
        widget = QWidget()
        self.window.add_perimeter_widget(widget, LEFT_POSITION)
        self.window.add_perimeter_widget(widget, RIGHT_POSITION)
        self.window.add_perimeter_widget(widget, TOP_POSITION)
        self.window.add_perimeter_widget(widget, BOTTOM_POSITION)

    def test_add_perimeter_widget_wrong_position(self):
        widget = QWidget()

        with self.assertRaises(KeyError):
            self.window.add_perimeter_widget(widget, WRONG_POSITION)

    def test_set_status(self):

        self.window.set_status(STATUS_TEXT)

        msg = self.window._status_bar.currentMessage()

        self.assertEqual(msg, STATUS_TEXT)

    def test_set_status_wrong_value(self):
        with self.assertRaises(TypeError):
            self.window.set_status(WRONG_STATUS_TEXT)

    # def create_menu(self):

    #     menu = self.window.add_menu(MENU_NAME)

    #     self.assertIsInstance(menu, Menu)

    # def test_add_menu(self):
    #     menu = self.window.add_menu(MENU_NAME, )

    # def test_add_menu_with_wrong_name(self):
    #     with self.assertRaises(TypeError):
    #         self.window.add_menu(WRONG_MENU_NAME)

    # def test_add_tool(self):
    #     with self.assertRaises(TypeError):
    #         self.window.add_tool('string')

    #     action = QAction(ACTION_NAME, self.window)
        
    #     self.window.add_tool(action)

    # def test_add_action(self):
    #     with self.assertRaises(TypeError):
    #         self.window.add_action('string')

    #     action = QAction(ACTION_NAME, self.window)
        
    #     self.window.add_action(action)

    # def test_remove_menu(self):
    #     with self.assertRaises(TypeError):
    #         self.window.remove_menu('string')

    #     with self.assertRaises(ValueError):
    #         menu = QMenu(MENU_NAME)
    #         self.window.remove_menu(menu)

    #     menu = QMenu(MENU_NAME)

    #     self.window.add_menu(menu)

    #     self.window.remove_menu(menu)

    # def test_remove_tool(self):
    #     with self.assertRaises(TypeError):
    #         self.window.remove_tool('string')

    #     with self.assertRaises(ValueError):
    #         action = QAction(ACTION_NAME, self.window)
    #         self.window.remove_tool(action)

    #     action = QAction(ACTION_NAME, self.window)

    #     self.window.add_tool(action)

    #     self.window.remove_tool(action)        

    # def test_remove_action(self):
    #     with self.assertRaises(TypeError):
    #         self.window.remove_action('string')

    #     with self.assertRaises(ValueError):
    #         action = QAction(ACTION_NAME, self.window)
    #         self.window.remove_action(action)

    #     action = QAction(ACTION_NAME, self.window)

    #     self.window.add_action(action)

    #     self.window.remove_action(action)

    def test_add_central_widget(self):
        widget = QWidget()

        self.window.add_central_widget(widget)

        with self.assertRaises(ValueError):
            self.window.add_central_widget(widget)

    def test_remove_central_widget(self):

        with self.assertRaises(ValueError):
            widget = QWidget()
            self.window.remove_central_widget(widget)

        widget = QWidget()

        self.window.add_central_widget(widget)

        self.window.remove_central_widget(widget)

    def test_close_current_widget(self):
        widget = QWidget()

        self.window.add_central_widget(widget)

        self.window._close_current_tab(0)

# if __name__ == '__main__':
#     unittest.main()
