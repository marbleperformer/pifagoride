import sys

import pytest

from PyQt5.QtWidgets import QApplication


def setup_module(module):

    module.app = QApplication(sys.argv)

    from guicore.appwindow.appwindow import AppWindow


def teardown_module(module):

    module.app.exit()

