from guicore.actions.actions import Action


TOOL_PREFIX = '_tool'
  

def add_tool(target, tool, prefix=TOOL_PREFIX):

    if not isinstance(tool, Action):

        raise TypeError('Wrong tool type, it should be %s.' % Action)

    tool_name = '_'.join((prefix, tool.name))

    setattr(target, tool_name, tool)

    target.addAction(tool)


def create_tool(target, name, func, title=None, icon=None, prefix=TOOL_PREFIX):

    tool =  Action(name, title, target, icon)

    tool.bind(func)

    tool_name = '_'.join((prefix, tool.name))

    setattr(target, tool_name, tool)

    target.addAction(tool)

    return tool


def all_tools(target):

    return filter(lambda itm: isinstance(itm, Action), target.__dict__.values())


def get_tool(target, name, prefix=TOOL_PREFIX):

    tool_name = '_'.join((prefix,  name))

    return getattr(target, tool_name)


def remove_tool(target, value, prefix=TOOL_PREFIX):

    name = value.name if isinstance(value, Action) else value

    tool_name = '_'.join((prefix, name))

    instance = getattr(target, tool_name)

    target.removeAction(instance.toolAction())

    delattr(target, tool_name)

