from PyQt5.QtWidgets import QMainWindow, QDesktopWidget, QTabWidget, QDockWidget, QMenuBar, QAction, QDialog

from PyQt5.QtCore import Qt

from guicore.menus.menus import Menu

from guicore.actions.actions import Action

# from guicore.menumanager.menumanager import MenuManagerMixin

# from guicore.toolmanager.toolmanager import ToolManagerMixin

from guicore.actions.managers import ActionManager

from guicore.menus.managers import MenuManager

from . import utility


class AppWindow(QMainWindow, MenuManager, ActionManager):

    LEFTDOC = Qt.LeftDockWidgetArea

    RIGHTDOCK = Qt.RightDockWidgetArea

    TOPDOC = Qt.TopDockWidgetArea

    BOTTOMDOC = Qt.BottomDockWidgetArea


    __menuclass__ = Menu


    __target__ = QMenuBar()


    def __init__(self, *args, **kwargs):

        super(AppWindow,self).__init__(*args, **kwargs)

        self.__render__()


    def __render__(self):

        self._tabs = QTabWidget()
        
        self._tabs.setMovable(True)

        self._tabs.setTabsClosable(True)

        self._status_bar = self.statusBar()

        self._tool_bar = self.addToolBar('Pifagor')

        self.setMenuBar(self.__target__)

        self.setCentralWidget(self._tabs)

        self.setGeometry(800, 600, 800, 600)

        self.setMinimumSize(256, 200)

        self.setWindowTitle('Pifagor IDE')

        dsk_widget = QDesktopWidget()

        geometry = dsk_widget.availableGeometry()

        center_position = geometry.center()

        frame_geometry = self.frameGeometry()

        frame_geometry.moveCenter(center_position)

        self.move(frame_geometry.topLeft())


    def set_status(self, text):

        self._status_bar.showMessage(text)


    def add_tab(self, widget, title=''):

        self._tabs.addTab(widget, title)


    def remove_tab(self, widget):

        idx = self._tabs.indexOf(widget)

        if widget.close():
            self._tabs.removeTab(idx)


    def add_doc(self, widget, position, title=''):

        dock = QDockWidget(title, self)

        dock.setWidget(widget)

        self.addDockWidget(position, dock)


    def add_tool(self, tool):

        utility.add_tool(self._tool_bar, tool)


    def create_tool(self, name, func, title=None, icon=None):

        return utility.create_tool(self._tool_bar, name, func, title, icon)


    def all_actions(self):

        return utility.all_actions(self._tool_bar)


    def get_action(self, name):

        return utility.get_action(self._tool_bar)


    def remove_action(self, value):

        utility.remove_action(value)
