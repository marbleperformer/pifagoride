from PyQt5.QtWidgets import QWidget, QMenu, QAction

from guicore.actions.actions import Menu, Action

from .appwindow import AppWindow

LEFT_POSITION = 'left'
RIGHT_POSITION = 'right'
TOP_POSITION = 'top'
BOTTOM_POSITION = 'bottom'

MENU_NAME = 'Menu'
MENU_REMOVE_NAME = 'Menu remove'
ACTION_NAME = 'Action'
ACTION_REMOVE_NAME = 'Action remove'

STATUS_TEXT = 'Status text'

ICON_PATH = 'static/Translate.png'

class NonClosableWidget(QWidget):
    def close(self):
        return False

def _show_(app):
    app.window = AppWindow()

    widget = QWidget()
    menu = Menu(MENU_NAME, app.window)
    action = Action(ACTION_NAME, app.window, ICON_PATH)

    widget_remove = QWidget()
    # menu_remove = QMenu(MENU_REMOVE_NAME, app.window)
    # action_remove = QAction(ACTION_REMOVE_NAME, app.window)

    app.window.add_perimeter_widget(widget, LEFT_POSITION, LEFT_POSITION)
    app.window.add_perimeter_widget(widget, RIGHT_POSITION, RIGHT_POSITION)
    app.window.add_perimeter_widget(widget, TOP_POSITION, TOP_POSITION)
    app.window.add_perimeter_widget(widget, BOTTOM_POSITION, BOTTOM_POSITION)

    app.window.add_menu(menu)
    # app.window.add_menu(menu_remove)
        
    app.window.add_tool(action)
    # app.window.add_tool(action_remove)

    app.window.add_action(action)
    # app.window.add_action(action_remove)

    # app.window.remove_menu(menu_remove)

    # app.window.remove_tool(action_remove)

    # app.window.remove_action(action_remove)

    app.window.add_central_widget(NonClosableWidget(), 'central')
    app.window.add_central_widget(widget_remove, 'remove')

    app.window.remove_central_widget(widget_remove)

    app.window.show()
