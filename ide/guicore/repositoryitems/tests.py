import os

import unittest

from PyQt5.QtGui import QStandardItemModel

from PyQt5.QtWidgets import QMainWindow

from .repositoryitems import RepositoryItem

from guicore.actions.actions import Action

from guicore.functionitems.functionitems import FunctionItem


ITEM_NAME = 'test_item'

ITEM_TITLE = 'test item'

WRONG_ITEM_NAME = 'wrong_item_name'

WRONG_REMOVE_VALUE = 123

ITEM_STRING = 'item'

ITEM_PATH = 'path/to/item/'


CHILD_FULL_NAME = '.'.join((ITEM_NAME, ITEM_NAME))

CHILD_PATH = os.path.join(ITEM_PATH, ITEM_NAME, ITEM_NAME)


ACTION_TITLE = 'Some action'

ACTION_NAME = 'some_action'

WRONG_ACTION_NAME = 'wrong_action_name'

ACTION_STRING = 'action'

ICON_PATH = 'static/Translate.png'

WRONG_REMOVE_VALUE = 123

STATIC_MENU_TITLE = 'static menu'


class DefaultChildrenRequired:

    testcase = None

    def __init__(self, func):

        self._func = func

    def __call__(self, *args, **kwargs):

        global default_item

        default_item = self.testcase.item.create_item(ITEM_TITLE, ITEM_PATH)

        return self._func(self.testcase, *args, **kwargs)


class RepositoryItemTestCase(unittest.TestCase):

    def setUp(self):

        model = QStandardItemModel()

        self.item = RepositoryItem(ITEM_TITLE, ITEM_PATH, model)


    def test_name(self):

        self.assertEqual(self.item.name, ITEM_NAME)


    def test_identity(self):

        self.assertEqual(self.item.identity, ITEM_PATH)


class ItemMangerImplementsTestCase(unittest.TestCase):

    def setUp(self):

        model = QStandardItemModel()

        self.item = RepositoryItem(ITEM_TITLE, ITEM_PATH, model)

        DefaultChildrenRequired.testcase = self


    def test_create_item(self):

        item = self.item.create_item(ITEM_TITLE, ITEM_PATH)
        
        attribute = self.item.get_item(ITEM_NAME)

        self.assertIs(item, attribute)


    @DefaultChildrenRequired
    def test_get_item(self):

        item = self.item.get_item(ITEM_NAME)

        self.assertIsNotNone(item)


    @DefaultChildrenRequired
    def test_get_wrong_item(self):

        with self.assertRaises(ValueError):

            self.item.get_item(WRONG_ITEM_NAME)


    def test_add_item(self):

        item = FunctionItem(ITEM_TITLE, ITEM_PATH, self.item)

        self.item.add_item(item)

        attribute = self.item.get_item(ITEM_NAME)

        self.assertIsNotNone(attribute)


    def test_add_wrong_item(self):

        with self.assertRaises(TypeError):

            self.item.add_item(ITEM_STRING)


    @DefaultChildrenRequired
    def test_remove_item(self):

        self.item.remove_item(ITEM_NAME)

        items = self.item.children

        self.assertEqual(len(tuple(items)), 0)


    @DefaultChildrenRequired
    def test_remove_item_by_item(self):

        attribute = self.item.get_item(ITEM_NAME)

        self.item.remove_item(value=attribute)

        items = self.item.children

        self.assertEqual(len(tuple(items)), 0)


    @DefaultChildrenRequired
    def test_remove_wrong_item(self):

        with self.assertRaises(TypeError):

            self.item.remove_item(WRONG_REMOVE_VALUE)

    @DefaultChildrenRequired
    def test_remove_not_exist_item(self):

        with self.assertRaises(ValueError):

            self.item.remove_item(WRONG_ITEM_NAME)

    @DefaultChildrenRequired
    def test_remove_not_exist_item_by_item(self):

        item = FunctionItem(WRONG_ITEM_NAME, ITEM_PATH, self.item)

        with self.assertRaises(ValueError):

            self.item.remove_item(item)


class DefaultStaticActionRequired:

    testcase = None

    def __init__(self, func):

        self._func =  func

    def __call__(self, *args, **kwargs):

        def action_function():
            pass

        global default_action

        parent = QMainWindow()

        default_action = Action(ACTION_TITLE, parent)

        self.testcase.manager.add_static_action(default_action) 

        return self._func(self.testcase, *args, **kwargs)


class StaticActionManagerImplementsTestCase(unittest.TestCase):

    def setUp(self):

        model = QStandardItemModel()

        self.manager = RepositoryItem(ITEM_TITLE, ITEM_PATH, model)

        DefaultStaticActionRequired.testcase = self


    @DefaultStaticActionRequired
    def test_get_actions(self):

        length = len(tuple(self.manager.get_static_actions()))

        self.assertGreater(length, 0)


    @DefaultStaticActionRequired
    def test_get_action(self):

        action = self.manager.get_static_action(ACTION_NAME)

        self.assertIsNotNone(action)


    def test_get_wron_action(self):

        with self.assertRaises(ValueError):

            self.manager.get_static_action(WRONG_ACTION_NAME)


    def test_add_static_action(self):

        parent = QMainWindow()

        action = Action(ACTION_NAME, parent)

        self.manager.add_static_action(action)

        attribute = self.manager.get_static_action(ACTION_NAME)


    def test_add_wrong_static_action(self):

        with self.assertRaises(TypeError):

            self.manager.add_static_action(ACTION_STRING)


    @DefaultStaticActionRequired
    def test_remove_static_action(self):

        self.manager.remove_static_action(ACTION_NAME)

        actions = self.manager.__staticactionbar__().actions()

        self.assertEqual(len(actions), 0)


    @DefaultStaticActionRequired
    def test_remove_static_action_by_action(self):

        action = self.manager.get_static_action(ACTION_NAME)

        self.manager.remove_static_action(value=action)

        actions = self.manager.__staticactionbar__().actions()

        self.assertEqual(len(actions), 0)


    @DefaultStaticActionRequired
    def test_remove_wrong_static_action(self):

        with self.assertRaises(TypeError):

            self.manager.remove_static_action(WRONG_REMOVE_VALUE)


    @DefaultStaticActionRequired
    def test_remove_not_exist_static_action(self):

        with self.assertRaises(ValueError):

            self.manager.remove_static_action(WRONG_ACTION_NAME)


    @DefaultStaticActionRequired
    def test_remove_not_exist_static_action_by_action(self):

        parent = QMainWindow()

        action = Action(WRONG_ACTION_NAME, parent)

        with self.assertRaises(ValueError):

            self.manager.remove_static_action(action)

