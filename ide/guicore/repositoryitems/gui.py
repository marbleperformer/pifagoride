from PyQt5.QtWidgets import QTreeView, QAbstractItemView

from PyQt5.QtGui import QStandardItemModel, QCursor

from PyQt5.QtCore import Qt

from .repositoryitems import RepositoryItem

from guicore.functionitems.functionitems import FunctionItem

from guicore.actions.actions import Action


REPO_TITLE = 'repository'

REPO_PATH = 'path/to/repo'

FUNCTION_TITLE = 'Function1'

FUNCTION_PATH = 'path/to/func'

ACTION_TITLE = 'context action'


def _show_(app):

    app.view = QTreeView()

    app.view.setContextMenuPolicy(Qt.CustomContextMenu)

    app.view.setEditTriggers(QAbstractItemView.NoEditTriggers)


    model = QStandardItemModel()

    app.view.setModel(model)


    repo = RepositoryItem(REPO_TITLE, REPO_PATH, model)

    repo = RepositoryItem(REPO_TITLE, REPO_PATH, model)

    repo = RepositoryItem(REPO_TITLE, REPO_PATH, model)


    func1 = FunctionItem(FUNCTION_TITLE, FUNCTION_PATH, repo)

    model.appendRow(repo)

    repo.add_item(func1)

    repo.create_item(FUNCTION_TITLE, FUNCTION_PATH)
    
    repo.create_item(FUNCTION_TITLE, FUNCTION_PATH)

    repo.create_item(FUNCTION_TITLE, FUNCTION_PATH)

    repo.create_item(FUNCTION_TITLE, FUNCTION_PATH)

    repo.create_item(FUNCTION_TITLE, FUNCTION_PATH)
    
    repo.create_item(FUNCTION_TITLE, FUNCTION_PATH)


    action = Action(ACTION_TITLE, app.view)

    RepositoryItem.add_static_action(action)

    FunctionItem.add_static_action(action)


    def open_menu():

        repo._menu.exec_(QCursor.pos())


    app.view.customContextMenuRequested.connect(open_menu)


    app.view.show()
