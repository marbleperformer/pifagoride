import os

from PyQt5.QtWidgets import QMenu

from PyQt5.QtGui import QStandardItem

from guicore.itemmanager.itemmanager import ItemManagerMixin

from guicore.actionmanager.actionmanager import StaticActionManagerMixin

from guicore.actions.actions import Action

from guicore.functionitems.functionitems import FunctionItem

class RepositoryItem(StaticActionManagerMixin, ItemManagerMixin, QStandardItem):


    def __new__(cls, *args, **kwargs):

        cls._menu = QMenu()

        return super().__new__(cls)


    def __init__(self, title, parent):

        super(RepositoryItem, self).__init__(title)

        self._parent = parent

        # self._path = path


    @staticmethod
    def __staticsuperaction__():

        return Action


    @classmethod
    def __staticactionbar__(cls):

        return cls._menu


    @classmethod
    def __statictarget__(cls):

        return cls


    def __superitem__(self):

        return FunctionItem
        
    
    def __itembar__(self):

        return self


    def __menubar__(self):

        return self.menu


    def __actionbar__(self):
        
        return self


    def __target__(self):

        return self


    @property
    def name(self):

        return self.text().replace(' ', '_')


    # @property
    # def identity(self):

    #     return self._path

