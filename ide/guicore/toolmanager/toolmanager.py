TOOL_SIGN = '_tool'

INTERFACE_ERROR_TEXT = '"%s" should be overrided before use.'

WRONG_TYPE_ERROR_TEXT = 'Wrong type argument "%s", it should be "%s".'

TOOL_NOT_EXITS_ERROR_TEXT = 'Tool "%s" not exist into "%s" context.'

NOT_ACTION_AND_STRING_ERROR_TEXT = 'Wrong type argument "value", it should be "%s" or "%s".'


class ToolManagerMixin:
    
    @staticmethod
    def __supertool__():

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__supertool__')


    def __toolbar__(self):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__toolbar__')


    def __target__(self):

        raise NotImplementedError(INTERFACE_ERROR_TEXT % '__target__')


    def get_tool(self, name):

        target = self.__target__()

        tool_name = name + TOOL_SIGN

        if not tool_name in target.__dict__:
            raise ValueError(TOOL_NOT_EXITS_ERROR_TEXT % (name, target))

        return getattr(target, tool_name)


    def create_tool(self, title, func, icon_path=None):

        tool_class = self.__supertool__()

        tool = tool_class(title, self, icon_path)

        tool.bind(func)

        tool_name = tool.name + TOOL_SIGN

        setattr(self.__target__(), tool_name, tool)

        self.__toolbar__().addAction(tool)

        return tool


    def add_tool(self, tool):

        if not isinstance(tool, self.__supertool__()):

            raise TypeError(WRONG_TYPE_ERROR_TEXT % ('tool', self.__supertool__()))

        tool_name = tool.name + TOOL_SIGN

        setattr(self.__target__(), tool_name, tool)
        
        self.__toolbar__().addAction(tool)


    def remove_tool(self, value):

        target = self.__target__()

        tool_class = self.__supertool__()

        if not isinstance(value, str) and not isinstance(value, tool_class):

            raise TypeError(NOT_ACTION_AND_STRING_ERROR_TEXT % (tool_class, str))

        tool_name = (value.name if isinstance(value, tool_class) else value) + TOOL_SIGN 


        if not tool_name in target.__dict__:

            raise ValueError(TOOL_NOT_EXITS_ERROR_TEXT % (tool_name, target))

        tool_instance = value if isinstance(value, tool_class) else self.get_tool(value)

        self.__toolbar__().removeAction(tool_instance)

        delattr(target, tool_name)

        return tool_instance
