import unittest

from PyQt5.QtWidgets import QMainWindow

from guicore.actions.actions import Action

from .toolmanager import ToolManagerMixin


TOOLBAR_NAME = 'Test'

TOOL_TITLE = 'Some tool'

TOOL_NAME = 'some_tool'

WRONG_TOOL_NAME = 'wrong_TOOL_NAME'

TOOL_STRING = 'tool'

ICON_PATH = 'static/Translate.png'

WRONG_REMOVE_VALUE = 123


class DefaultToolRequired:

    testcase = None

    def __init__(self, func):

        self._func =  func

    def __call__(self, *args, **kwargs):

        def tool_function():
            pass

        global default_tool

        default_tool = self.testcase.manager.create_tool(TOOL_TITLE, tool_function, ICON_PATH) 

        return self._func(self.testcase, *args, **kwargs)


class ToolManagerMixinTestCase(unittest.TestCase):

    def setUp(self):

        class Manager(ToolManagerMixin, QMainWindow):

            def __init__(self):

                super(Manager, self).__init__()

                self.toolbar = self.addToolBar(TOOLBAR_NAME)

            @staticmethod
            def __supertool__():

                return Action

            def __toolbar__(self):

                return self.toolbar

            def __target__(self):

                return self

        self.manager = Manager()

        DefaultToolRequired.testcase = self


    def test_not_implemented_supertool(self):

        class Manager(ToolManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__supertool__()


    def test_not_implemented_toolbar(self):

        class Manager(ToolManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__toolbar__()


    def test_not_implemented_target(self):

        class Manager(ToolManagerMixin):
            pass

        manager = Manager()

        with self.assertRaises(NotImplementedError):

            manager.__target__()


    def test_create_tool(self):

        def tool_function():
            pass

        tool = self.manager.create_tool(TOOL_TITLE, tool_function)
        
        attribute = self.manager.get_tool(TOOL_NAME)

        self.assertIs(tool, attribute)


    def test_create_tool_with_icon(self):

        def tool_function():
            pass

        tool = self.manager.create_tool(TOOL_TITLE, tool_function, ICON_PATH)

        attribute = self.manager.get_tool(TOOL_NAME)

        self.assertIs(tool, attribute)


    @DefaultToolRequired
    def test_get_tool(self):

        tool = self.manager.get_tool(TOOL_NAME)

        self.assertIsNotNone(tool)


    @DefaultToolRequired
    def test_get_wrong_tool(self):

        with self.assertRaises(ValueError):

            self.manager.get_tool(WRONG_TOOL_NAME)


    def test_add_tool(self):

        action = Action(TOOL_TITLE, self.manager)

        self.manager.add_tool(action)

        attribute = self.manager.get_tool(TOOL_NAME)

        self.assertIsNotNone(attribute)


    def test_add_wrong_action(self):

        with self.assertRaises(TypeError):

            self.manager.add_tool(TOOL_STRING)


    @DefaultToolRequired
    def test_remove_tool(self):

        self.manager.remove_tool(TOOL_NAME)

        tools = self.manager.__toolbar__().actions()

        self.assertEqual(len(tools), 0)


    @DefaultToolRequired
    def test_remove_tool_by_tool(self):

        attribute = self.manager.get_tool(TOOL_NAME)

        self.manager.remove_tool(value=attribute)

        tools = self.manager.__toolbar__().actions()

        self.assertEqual(len(tools), 0)


    @DefaultToolRequired
    def test_remove_wrong_tool(self):

        with self.assertRaises(TypeError):

            self.manager.remove_tool(WRONG_REMOVE_VALUE)

    @DefaultToolRequired
    def test_remove_not_exist_tool(self):

        with self.assertRaises(ValueError):

            self.manager.remove_tool(WRONG_TOOL_NAME)

    @DefaultToolRequired
    def test_remove_not_exist_tool_by_tool(self):

        tool = Action(WRONG_TOOL_NAME, self.manager)

        with self.assertRaises(ValueError):

            self.manager.remove_tool(tool)
