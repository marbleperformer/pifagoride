import os, sys, re, json

from collections import OrderedDict

from PyQt5.QtWidgets import QApplication, QDialog, QGridLayout, QHBoxLayout, QPushButton, QLineEdit, QLabel
from PyQt5.QtCore import pyqtSlot, Qt

DEFAULT_FUNC_NAME = 'pifagor_function'
DEFAULT_RANG_VALUE = '00'

COMPONENTS_SECTION_NAME = 'components'
FUNC_MAP_NAME = 'pfg'

FUNCTION_FORMAT = '{url}/{name}/{rang}/1.pfg'

FUNCTION_PATTERN = r'^(?P<body>(?P<url>(?P<parent_url>.+\/)(?P<name>\w+)\/(?P<rang>\d{2}\.\d{2}))\/1\.pfg)$'

class Funcbuilder(QDialog):

    attrs = dict()

    def __init__(self, url):
        super().__init__()

        self.url = url

        lay = QGridLayout(self)
        top_lay = QGridLayout()
        func_lay = QHBoxLayout()

        btn_lay = QGridLayout()
        accept_btn = QPushButton('Accept')
        reject_btn = QPushButton('Reject')

        self.name = QLineEdit(DEFAULT_FUNC_NAME)

        self.path = QLabel(self._get_full_path())

        func_lay.addWidget(self.name)

        btn_lay.addWidget(accept_btn, 0, 0)
        btn_lay.addWidget(reject_btn, 0, 1)

        top_lay.addWidget(QLabel('Name'), 0, 0)
        top_lay.addWidget(QLabel('Path'), 1, 0)
        top_lay.addLayout(func_lay, 0, 1)
        top_lay.addWidget(self.path, 1, 1)

        lay.addLayout(top_lay, 0, 0)
        lay.addLayout(btn_lay, 1, 0, Qt.AlignRight)

        self.name.textChanged.connect(self.set_full_path)
        reject_btn.clicked.connect(self.reject_click)
        accept_btn.clicked.connect(self.accept_click)

    @pyqtSlot(str)
    def set_full_path(self, text):
        self.path.setText(self._get_full_path())

    def _get_full_path(self):
        rang = '00.00'
        # url = os.path.dirname(self.url)
        # rang = '.'.join((self.major_rang.text(), self.minor_rang.text()))
        return FUNCTION_FORMAT.format(url=self.url, name=self.name.text(), rang=rang)

    def reject_click(self):
        Funcbuilder.attrs = None
        self.reject()

    def accept_click(self):
        match = re.fullmatch(FUNCTION_PATTERN, self._get_full_path())
        Funcbuilder.attrs = match.groupdict()
        self.accept()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    widget = Funcbuilder('fileserializer.conf', 'Tree', '')
    widget.show()
    sys.exit(app.exec_())
