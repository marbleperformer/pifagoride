from collections import Callable
from PyQt5.QtWidgets import QDialog, QGridLayout, QPushButton
from PyQt5.QtCore import Qt

class WrapDialog(QDialog):
	def __init__(self, widget, accept = None, reject = None):
		super().__init__()
		lay = QGridLayout(self)
		self.btn_lay = QGridLayout()
		self.accept_btn = QPushButton('Accept')
		self.reject_btn = QPushButton('Reject')
		if accept:
			self.set_accept_method(accept)
		if reject:
			if isinstance(reject, Callable):
				self.set_reject_method(reject)
			else:
				self.reject_btn.clicked.connect(self.reject)
				self.btn_lay.addWidget(self.reject_btn, 0,1)
		lay.addWidget(widget, 0, 0)
		lay.addLayout(self.btn_lay, 1,0,Qt.AlignRight)

	def set_accept_method(self, method):
		def accept_wrap():
			method()
			self.accept()
		accept_btn = QPushButton('Accept')
		accept_btn.clicked.connect(accept_wrap)
		self.btn_lay.addWidget(accept_btn, 0, 0)

	def set_reject_method(self, method):
		def reject_wrap():
			method()
			self.reject()
		reject_btn = QPushButton('Reject')
		reject_btn.clicked.connect(reject_wrap)
		self.btn_lay.addWidget(reject_btn, 0, 1)