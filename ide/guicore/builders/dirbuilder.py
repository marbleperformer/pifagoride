import os
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QGridLayout, QHBoxLayout, QPushButton, QFileDialog

import settings

class DirectoryBuilder(QWidget):

	path = str()

	def __init__(self):
		super().__init__()

		lay = QGridLayout(self)
		path_lay = QHBoxLayout()

		self.name = QLineEdit()
		self.path = QLineEdit(settings.SELF_DIR_PATH)

		path_btn = QPushButton('Browse')

		path_lay.addWidget(self.path)
		path_lay.addWidget(path_btn)

		lay.addWidget(QLabel('Name'), 0, 0)
		lay.addWidget(QLabel('Path'), 1, 0)

		lay.addWidget(self.name, 0, 1)
		lay.addLayout(path_lay, 1, 1)

		path_btn.clicked.connect(self.path_click)

	def path_click(self):
		path = QFileDialog.getExistingDirectory(self, 'Choose directory')
		if path:
			self.path.setText(path)

	# TODO add field validation
	# TODO add path validation
	def save(self):
		path = os.path.join(self.path.text(), self.name.text())
		os.makedirs(path)
		DirectoryBuilder.path = path