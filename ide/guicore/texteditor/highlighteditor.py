import sys, os
from PyQt5.QtWidgets import QApplication, QTextEdit, QAction
from PyQt5.QtGui import QTextCharFormat, QSyntaxHighlighter, QColor, QCursor, QKeySequence
from PyQt5.QtCore import QRegExp, Qt

from collections import OrderedDict

class Highlighter(QSyntaxHighlighter):
	def __init__(self, document):
		super().__init__(document)
		self.key_words = ['funcdef']

	def highlightBlock(self, text):
		test_format = QTextCharFormat()
		test_format.setForeground(QColor("red"))
		for key in self.key_words:
			expression = QRegExp(key)
			index = expression.indexIn(text, 0)
			while index >= 0:
				index = expression.pos(0)
				length = len(expression.cap(0))
				self.setFormat(index, length, test_format)
				index = expression.indexIn(text, index + length)

class HighlightEdit(QTextEdit):
	def __init__(self, **attrs):
		super(QTextEdit, self).__init__()

		self.menus = OrderedDict()
		self.actions = OrderedDict()

		self.__dict__.update(attrs)

		self.setLineWrapMode(QTextEdit.NoWrap)
		self.highliter = Highlighter(self.document())

		self.setContextMenuPolicy(Qt.CustomContextMenu)
		self.customContextMenuRequested.connect(self.open_menu)

	@property
	def identity(self):
		return self.url

	def open_menu(self):
		self.menu = self.createStandardContextMenu(QCursor.pos())
		if self.menus:
			self.menu.addSeparator()
		for menu in self.menus.values():
			self.menu.addMenu(menu)

		if self.actions:
			self.menu.addSeparator()
		for action in self.actions.values():
			self.menu.addAction(action)
		self.menu.exec_(QCursor.pos())

	def setContextAction(self, title, action):
		self.actions[title] = action

	def setContextMenu(self, title, menu):
		self.menus[title] = menu
