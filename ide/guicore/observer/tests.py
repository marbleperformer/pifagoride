import os

import logging

import unittest

from .observer import Observer, Actor

TEST_TARGET_NAME = 'testtarget'

UPDATE_METHOD_NAME = 'update'

TEST_UPDATE_TEXT = 'this is a text update text!'

TEST_NOTIFICATE_TEXT = 'this is a text notificate text!'

LOG_FILE_NAME = 'gui_observer.log'

FULL_LOG_PATH = os.path.join(os.getcwd(), LOG_FILE_NAME)

LOGGER = logging.getLogger('gui_observer')
LOGGER.setLevel(logging.DEBUG)

FORMATTER = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s ")

HANDLER = logging.FileHandler(LOG_FILE_NAME, encoding='utf-8')
HANDLER.setLevel(logging.DEBUG)

HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(HANDLER)

class TestActor(Actor):
    pass

class TestObserver(Observer):
    def testtarget_update_handler(self, data):
        LOGGER.info('hotificated: %s' % data)

class ObserverTestCase(unittest.TestCase):
    def setUp(self):
        self.observer = TestObserver()

    def test_update(self):
        self.observer.update(TEST_TARGET_NAME, UPDATE_METHOD_NAME, TEST_UPDATE_TEXT)

        self.assertEqual(os.path.exists(FULL_LOG_PATH), True)

class ActorTestCase(unittest.TestCase):
    def setUp(self):
        self.actor = TestActor()

    def test_add_observer(self):
        observer = TestObserver()

        self.actor.add_observer(observer)

        self.assertEqual(len(self.actor._observers), 1)

    def test_remove_observer(self):
        with self.assertRaises(ValueError):
            observer = TestObserver()
            self.actor.remove_observer(observer)

        observer = TestObserver()

        self.actor.add_observer(observer)

        self.actor.remove_observer(observer)

        self.assertEqual(len(self.actor._observers), 0)

    def test_notificate(self):
        observer = TestObserver()

        self.actor.add_observer(observer)

        self.actor.notificate(TEST_TARGET_NAME, UPDATE_METHOD_NAME, TEST_NOTIFICATE_TEXT)

if __name__ == '__main__':
    unittest.main()
