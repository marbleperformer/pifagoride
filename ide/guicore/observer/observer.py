OBSERVER_NOT_EXISTS_ERROR_TEXT = ''

HANDLER_SIGN = 'handler' 

class Observer:
    def update(self, target, method, data):
        handler_name = '_'.join((target.lower(), method.lower(), HANDLER_SIGN))
        if handler_name in dir(self):
            handler = getattr(self, handler_name)
            handler(data)

class Actor:
    def __init__(self, *args, **kwargs):
        super(Actor, self).__init__(*args, **kwargs)
        self._observers = set()

    def add_observer(self, observer):
        self._observers.add(observer)

    def remove_observer(self, observer):
        if not observer in self._observers:
            raise ValueError(OBSERVER_NOT_EXISTS_ERROR_TEXT)

        self._observers.remove(observer)

    def notificate(self, target, method, data):
        for observer in self._observers:
            observer.update(target, method, data)
