from PyQt5.QtWidgets import QTextEdit


class SimpleViewer(QTextEdit):

    def __init__(self, *args, **kwargs):

        super(SimpleViewer, self).__init__(*args, **kwargs)

        self.setReadOnly(True)


    def __update__(self, data):

        self.setText(data)
