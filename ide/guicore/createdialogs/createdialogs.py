from PyQt5.QtWidgets import QDialog, QFileDialog, QGridLayout, QHBoxLayout, QPushButton, QLineEdit, QLabel

from PyQt5.QtCore import Qt


FUNCTION_IDENTITY_MASK = '{name}/{major}.{minor}'


class RepositoryDialog(object):

    def open_repository():

        return QFileDialog.getExistingDirectory()
        


class CreateLocalFunctionDialog(QDialog):

    def __init__(self):

        super(CreateLocalFunctionDialog, self).__init__()

        lay = QGridLayout(self)

        top_lay = QGridLayout()

        rang_lay = QGridLayout()

        func_lay = QHBoxLayout()

        btn_lay = QGridLayout()

        accept_btn = QPushButton('Accept')

        reject_btn = QPushButton('Reject')

        self.name = QLineEdit()

        self.path = QLabel()

        func_lay.addWidget(self.name)

        btn_lay.addWidget(accept_btn, 0, 0)

        btn_lay.addWidget(reject_btn, 0, 1)

        top_lay.addWidget(QLabel('Name'), 0, 0)

        top_lay.addWidget(QLabel('Path'), 1, 0)

        top_lay.addLayout(func_lay, 0, 1)

        top_lay.addWidget(self.path, 1, 1)

        self.major_rang = QLineEdit()

        self.minor_rang = QLineEdit()

        rang_lay.addWidget(self.major_rang, 0, 1)

        rang_lay.addWidget(self.minor_rang, 0, 2)

        lay.addWidget(QLabel('Name'), 0, 0)

        lay.addWidget(self.name, 0, 1)

        lay.addWidget(QLabel('Rang'), 1, 0)

        lay.addLayout(rang_lay, 1, 1)

        lay.addWidget(QLabel(), 2, 0)

        lay.addLayout(btn_lay, 3, 1, Qt.AlignRight)

        accept_btn.clicked.connect(self.accept_click)




    @classmethod
    def show_dialog(cls):

        global func_name

        global major_rang

        global minor_rang

        dialog = cls()

        dialog.exec_()

        print(func_name)

        # return FUNCTION_IDENTITY_MASK.format(name=func_name, major=major_rang, minor=minor_rang)


    # @classmethod
    # def get_arguments(cls):

    #     global name

    #     global path

    #     global rang

    #     dialog = cls()

    #     dialog.show()


    def accept_click(self):

        func_name = self.name.text()

        major_rang = self.major_rang.text()

        minor_rang = self.minor_rang.text()

        self.accept()
