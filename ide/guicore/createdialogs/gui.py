import sys

from .createdialogs import CreateLocalFunctionDialog

def _show_(app):

    app.dialog = CreateLocalFunctionDialog()

    print(app.dialog.show_dialog())
