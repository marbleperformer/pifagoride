import sys

import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("-t", "--function")

    args = parser.parse_args()

    sys.stdout.write('Translation %s' % args.function)
